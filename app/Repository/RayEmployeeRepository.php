<?php

namespace App\Repository;

use App\Interfaces\RayEmployees\RayEmployeeRepositoryInterface;
use App\Models\Invoice;
use App\Models\Patient;
use App\Models\PatientAccount;
use App\Models\Payment;
use App\Models\RayEmployee;
use Illuminate\Support\Facades\Hash;

class RayEmployeeRepository implements RayEmployeeRepositoryInterface
{
    public function index()
    {
        $RayEmployees = RayEmployee::all();
        return view('dashboard.RayEmployee.index',compact('RayEmployees'));
    }

    public function store($data)
    {
        RayEmployee::create([
            'full_name' => $data['full_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']) ,

        ]);
        return redirect()->route('RayEmployees.index');
    }


    public function update($data, $id)
    {
        $RayEmployee = RayEmployee::findOrFail($id);
        $RayEmployee->update([
            'full_name' => $data['full_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']) ,
        ]);
        return redirect()->route('RayEmployees.index');
    }

    public function destroy($id)
    {
        $RayEmployee = RayEmployee::findOrFail($id);
        $RayEmployee->delete();
        return redirect()->route('RayEmployees.index');
    }


}
