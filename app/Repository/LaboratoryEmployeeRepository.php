<?php

namespace App\Repository;

use App\Interfaces\LaboratoryEmployee\LaboratoryEmployeeRepositoryInterface;
use App\Models\Invoice;
use App\Models\Patient;
use App\Models\PatientAccount;
use App\Models\Payment;
use App\Models\LaboratorieEmployee;
use Illuminate\Support\Facades\Hash;

class LaboratoryEmployeeRepository implements LaboratoryEmployeeRepositoryInterface
{
    public function index()
    {
        $LaboratoryEmployees = LaboratorieEmployee::all();
        return view('dashboard.LaboratoryEmployee.index',compact('LaboratoryEmployees'));
    }



    public function store($data)
    {
        LaboratorieEmployee::create([
            'full_name' => $data['full_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']) ,

        ]);
        return redirect()->route('LaboratoryEmployees.index');
    }


    public function update($data, $id)
    {
        $LaboratorieEmployee = LaboratorieEmployee::findOrFail($id);
        $LaboratorieEmployee->update([
            'full_name' => $data['full_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']) ,
        ]);
        return redirect()->route('LaboratoryEmployees.index');
    }

    public function destroy($id)
    {
        $LaboratorieEmployee = LaboratorieEmployee::findOrFail($id);
        $LaboratorieEmployee->delete();
        return redirect()->route('LaboratoryEmployees.index');
    }


}
