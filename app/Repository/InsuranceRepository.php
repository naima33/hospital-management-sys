<?php

namespace App\Repository;
use App\Interfaces\Insurances\InsuranceRepositoryInterface;
use App\Models\Assurance;
use Throwable;

class InsuranceRepository implements InsuranceRepositoryInterface
{
    public function index()
    {
        $insurances=Assurance::all();
        return view('dashboard.Insurances.index',compact('insurances'));
    }

    public function store($data)
    {
        Assurance::create([
            'notes'=>$data['notes'],
            'name'=>$data['insurance_name'],
            'discount_percentage'=>$data['discount_percentage'],
            'company_rate'=>$data['company_rate'],
        ]);
        return redirect()->route('Insurances.index');
    }

    public function create()
    {
        $insurances=Assurance::all();
        return view('dashboard.Insurances.add');
    }

    public function update($data,$id)
    {
        $Assurance=Assurance::findOrFail($id);
        $Assurance->update([
            'notes'=>$data['notes'],
            'name'=>$data['insurance_name'],
            'discount_percentage'=>$data['discount_percentage'],
            'company_rate'=>$data['company_rate'],
            'status'=>$data['status']==='on'? 1:0,
        ]);
        return redirect()->route('Insurances.index');
    }

    public function insuranceActive($data, $id)
    {
        try {
            $Assurance = Assurance::findOrFail($id);
            $Assurance->update([
                'status' => $data['status'] === 'on' ? 1 : 0,
            ]);
            return redirect()->route('Insurances.index');
        } catch (Throwable $e) {

            throw $e;
        }
    }

    public function destroy($id){
        $Assurance=Assurance::findOrFail($id);
        $Assurance->delete();
        return redirect()->route('Insurances.index');
    }

}
