<?php

namespace App\Repository;
use App\Interfaces\Sections\SectionRepositoryInterface;
use App\Models\Section;

class SectionRepository implements SectionRepositoryInterface
{
    public function index()
    {
        $sections=Section::all();
        return view('dashboard.Sections.index',compact('sections'));
    }

    public function store($data)
    {
        Section::create([
            'name'=>$data['name'],
            'description'=>$data['description'],
        ]);
        return redirect()->route('Sections.index');
    }

    public function update($data,$id)
    {
        $section=Section::findOrFail($id);
        $section->update([
            'name'=>$data['name'],
            'description'=>$data['description'],
        ]);
        return redirect()->route('Sections.index');
    }

    public function destroy($id){
        $section=Section::findOrFail($id);
        $section->delete();
        return redirect()->route('Sections.index');
    }

    public function show($id){
        $doctors=Section::findOrFail($id)->doctors;
        return view('dashboard.Sections.show_doctor',compact('doctors'));
    }
}
