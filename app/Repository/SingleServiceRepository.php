<?php

namespace App\Repository;
use App\Interfaces\Services\SingleServiceRepositoryInterface;
use App\Models\SingleService;

class SingleServiceRepository implements SingleServiceRepositoryInterface
{
    public function index()
    {
        $SingleService=SingleService::all();
        return view('dashboard.Services.Single_services.index',compact('SingleService'));
    }

    public function create()
    {
        return view('dashboard.Services.single_services.add');
    }

    public function store($data)
    {
        SingleService::create([
            'name'=>$data['name'],
            'price'=>$data['price'],
            'status'=> 1,
            'description'=>$data['description'],
        ]);
        return redirect()->route('SingleServices.index');
    }

    public function update($data,$id)
    {
        $SingleService=SingleService::findOrFail($id);
        $SingleService->update([
            'name'=>$data['name'],
            'price'=>$data['price'],
            'status'=>$data['status']==='on'? 1:0,
            'description'=>$data['description'],
        ]);
        return redirect()->route('SingleServices.index');
    }

    public function destroy($id){
        $SingleService=SingleService::findOrFail($id);
        $SingleService->delete();
        return redirect()->route('SingleServices.index');
    }

}
