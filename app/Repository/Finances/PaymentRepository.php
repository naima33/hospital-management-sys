<?php

namespace App\Repository\Finances;

use App\Interfaces\Finances\PaymentRepositoryInterface;
use App\Models\Payment;
use App\Models\FundAccount;
use App\Models\Patient;
use App\Models\PatientAccount;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Elibyy\TCPDF\Facades\TCPDF;
class PaymentRepository implements PaymentRepositoryInterface
{
    public function index()
    {
        $Payments = Payment::all();
        return view('dashboard.Finances.Payments.index', compact('Payments'));
    }

    public function create()
    {
        $patients = Patient::all();
        return view('dashboard.Finances.Payments.add', compact('patients'));
    }

    public function store($data)
    {
        DB::beginTransaction();
        try{
            $Payment=Payment::create([
                'date' => $data['date'],
                'patient_id' => $data['patient_id'],
                'amount' => $data['amount'],
                'description' => $data['description'],

            ]);
            $FundAccount=FundAccount::create([
                'date' => Carbon::now(),
                'payment_id' =>$Payment->id,
                'debit' => 0.00,
                'credit' => $data['amount'],
            ]);

            $PatientAccount=PatientAccount::create([
                'date' => Carbon::now(),
                'payment_id' => $Payment->id,
                'patient_id' => $data['patient_id'],
                'debit' => $data['amount'],
                'credit' =>  0.00,
            ]);

            DB::commit();
            return redirect()->route('Payments.index');
        }
        catch(\Exception $e){
            DB::rollBack();
            throw $e;
        }
    }

    public function edit($id)
    {
        $Payment = Payment::findOrFail($id);
        $patients = Patient::all();
        return view('dashboard.Finances.Payments.edit', compact('Payment','patients'));
    }

    public function update($data, $id)
    {
        DB::beginTransaction();
        try{
            $Payment = Payment::findOrFail($id);
            $Payment->update([
                'date' => $data['date'],
                'patient_id' => $data['patient_id'],
                'amount' => $data['amount'],
                'description' => $data['description'],

            ]);
            $FundAccount = FundAccount::where('payment_id',$id)->first();
            $FundAccount->update([
                'date' => Carbon::now(),
                'payment_id' => $Payment->id,
                'debit' => 0.00,
                'credit' => $data['amount'],
            ]);
            $PatientAccount = PatientAccount::where('payment_id',$id)->first();
            $PatientAccount->update([
                'date' => Carbon::now(),
                'payment_id' => $Payment->id,
                'patient_id' => $data['patient_id'],
                'debit' => $data['amount'],
                'credit' =>  0.00,
            ]);

            DB::commit();
            return redirect()->route('Payments.index');
        }
        catch(\Exception $e){
            DB::rollBack();
            throw $e;
        }

    }

    public function destroy($id)
    {
        $Payment = Payment::findOrFail($id);
        $Payment->delete();
        return redirect()->route('Payments.index');
    }

     public function PrintBill($id)
    {
        return $id;
        // $result=[
        //     'bill_info'      => $bill,
        //     'item_movement'  => $bill?->itemMovement,
        //     'total_price'    => abs($bill?->itemMovement->quantity*$bill?->itemMovement?->item->price),
        //     'date'           =>  Carbon::parse($bill->created_at)->toDateTimeString(),
        // ];

        //    $filename = 'Bill.pdf';
        //    $view = View::make('Bill', ['results' => $result]);
        //    $html = $view->render();

        //    $pdf = new TCPDF;

        //    $pdf::SetTitle('Bill');
        //    $pdf::SetFont('aealarabiya', '', 18);
        //    $pdf::setRTL(true);
        //    $pdf::AddPage();

        //    $pdf::writeHTML($html, true, false, true, false, '');

        //    $pdf::Output(public_path($filename), 'F');

        //    return response()->download(public_path($filename));
    }

    public function show($id){
        $Payment=Payment::findOrFail($id);
        return view('dashboard.Finances.Payments.print', compact('Payment'));
    }
}
