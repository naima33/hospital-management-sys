<?php

namespace App\Repository\Finances;

use App\Interfaces\Finances\ReceiptRepositoryInterface;
use App\Models\Receipt;
use App\Models\FundAccount;
use App\Models\Patient;
use App\Models\PatientAccount;
use Carbon\Carbon;
use Elibyy\TCPDF\Facades\TCPDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class ReceiptRepository implements ReceiptRepositoryInterface
{
    public function index()
    {
        $Receipts = Receipt::all();
        return view('dashboard.Finances.Receipts.index', compact('Receipts'));
    }

    public function create()
    {
        $patients = Patient::all();
        return view('dashboard.Finances.Receipts.add', compact('patients'));
    }

    public function store($data)
    {
        DB::beginTransaction();
        try{
            $receipt=Receipt::create([
                'date' => $data['date'],
                'patient_id' => $data['patient_id'],
                'amount' => $data['amount'],
                'description' => $data['description'],

            ]);

            FundAccount::create([
                'date' => Carbon::now(),
                'receipt_id' => $receipt->id,
                'debit' => $data['amount'],
                'credit' => 0.00,
            ]);


            PatientAccount::create([
                'date' => Carbon::now(),
                'receipt_id' => $receipt->id,
                'patient_id' => $data['patient_id'],
                'debit' => 0.00,
                'credit' =>  $data['amount'],
            ]);
            DB::commit();
            return redirect()->route('Receipts.index');
        }
        catch(\Exception $e){
            DB::rollBack();
            throw $e;
        }


    }

    public function edit($id)
    {
        $Receipt = $Receipt = Receipt::with('patient')->findOrFail($id);
        $patients = Patient::all();
        return view('dashboard.Finances.Receipts.edit',compact('Receipt','patients'));
    }

    public function update($data, $id)
    {

        DB::beginTransaction();
        try{
            $Receipt = Receipt::findOrFail($id);
            $Receipt->update([
                'date' => $data['date'],
                'patient_id' => $data['patient_id'],
                'amount' => $data['amount'],
                'description' => $data['description'],

            ]);
            $FundAccount = FundAccount::where('receipt_id',$id)->first();
            $FundAccount->update([
                'date' => Carbon::now(),
                'receipt_id' => $Receipt->id,
                'debit' => $data['amount'],
                'credit' => 0.00,
            ]);
            $PatientAccount = PatientAccount::where('receipt_id',$id)->first();
            $PatientAccount->update([
                'date' => Carbon::now(),
                'receipt_id' => $Receipt->id,
                'patient_id' =>$data['patient_id'],
                'debit' => 0.00,
                'credit' =>  $data['amount'],
            ]);

            DB::commit();
            return redirect()->route('Receipts.index');
        }
        catch(\Exception $e){
            DB::rollBack();
            throw $e;
        }
    }

    public function destroy($id)
    {
        $Receipt = Receipt::findOrFail($id);
        $Receipt->delete();
        return redirect()->route('Receipts.index');
    }

    public function PrintBill($id)
    {
        $Receipt=Receipt::findOrFail($id);
        $result=[
            'id'                => $Receipt->id,
            'patient_name'      => $Receipt->patient->full_name,
            'amount'            => $Receipt->amount,
            'date'              =>  $Receipt->date,
        ];

           $filename = 'Receipt.pdf';
           $view = View::make('dashboard.Finances.Receipts.print', ['result' => $result]);
           $html = $view->render();

           $pdf = new TCPDF;

           $pdf::SetTitle('Receipt');
           $pdf::SetFont('aealarabiya', '', 18);
           $pdf::setRTL(true);
           $pdf::AddPage();

           $pdf::writeHTML($html, true, false, true, false, '');

           $pdf::Output(public_path($filename), 'F');

           return response()->download(public_path($filename));
    }

    public function show($id){
        $Receipt=Receipt::findOrFail($id);
        return view('dashboard.Finances.Receipts.show', compact('Receipt'));
    }
}
