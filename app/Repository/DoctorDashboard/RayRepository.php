<?php

namespace App\Repository\DoctorDashboard;
use App\Interfaces\DoctorDashboard\RayRepositoryInterface;
use App\Models\Ray;

class RayRepository implements RayRepositoryInterface
{
    public function index()
    {
        $rays=Ray::all();
        return view('dashboard.Rays.index',compact('rays'));
    }

    public function store($data)
    {
        try{
            Ray::create([
                'description'=>$data['description'],
                'patient_id'=>$data['patient_id'],
                'doctor_id'=>$data['doctor_id'],
                'invoice_id'=>$data['invoice_id'],

            ]);
            return redirect()->route('Invoices.index');
        }
        catch(\Exception $e)
        {
            throw $e;
        }
    }

    public function update($data,$id)
    {
        try{
            $ray=Ray::findOrFail($id);
            $ray->update([
                'description'=>$data['description'],

            ]);
            return redirect()->route('Invoices.index');
        }
        catch(\Exception $e)
        {
            throw $e;
        }
    }

    public function destroy($id)
    {
        try{
            $ray=Ray::findOrFail($id);
            $ray->delete();
            return redirect()->route('Invoices.index');
        }
        catch(\Exception $e){

            throw $e;
        }
    }


}
