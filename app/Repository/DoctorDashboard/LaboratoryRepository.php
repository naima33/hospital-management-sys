<?php

namespace App\Repository\DoctorDashboard;
use App\Interfaces\DoctorDashboard\LaboratoryRepositoryInterface;
use App\Models\Laboratory;

class LaboratoryRepository implements LaboratoryRepositoryInterface
{

    public function store($data)
    {
        try{
            Laboratory::create([
                'description'=>$data['description'],
                'patient_id'=>$data['patient_id'],
                'doctor_id'=>$data['doctor_id'],
                'invoice_id'=>$data['invoice_id'],

            ]);
            return redirect()->route('Invoices.index');
        }
        catch(\Exception $e)
        {
            throw $e;
        }
    }

    public function update($data,$id)
    {
        try{
            $lab=Laboratory::findOrFail($id);
            $lab->update([
                'description'=>$data['description'],

            ]);
            return redirect()->route('Invoices.index');
        }
        catch(\Exception $e)
        {
            throw $e;
        }
    }

    public function destroy($id){
        try
        {
            $laboratory=Laboratory::findOrFail($id);
            $laboratory->delete();
            return redirect()->route('Laboratories.index');
        }
        catch(\Exception $e)
        {
            throw $e;
        }
    }


}
