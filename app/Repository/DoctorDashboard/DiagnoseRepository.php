<?php

namespace App\Repository\DoctorDashboard;

use App\Interfaces\DoctorDashboard\DiagnosisRepositoryInterface;
use App\Models\Appointment;
use App\Models\Diagnose;
use App\Models\Invoice;
use App\Models\Section;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DiagnoseRepository implements DiagnosisRepositoryInterface
{

    public function store($data)
    {
        DB::beginTransaction();
        try{
            $invoiceStatus=Invoice::findorFail($data['invoice_id']);
            $invoiceStatus->update([
                 'invoice_status'=>3,
            ]);
            Diagnose::create([
                'patient_id'=>$data['patient_id'],
                'doctor_id'=>$data['doctor_id'],
                'invoice_id'=>$data['invoice_id'],
                'diagnose'=>$data['diagnosis'],
                'medicine'=>$data['medicine'],
                'date' => Carbon::now(),

            ]);
            DB::commit();
            return redirect()->route('Invoices.index');
        }
        catch(\Exception $e)
        {
            DB::rollBack();
            throw $e;
        }
    }

    public function add_review($data)
    {
        DB::beginTransaction();
        try{
            $invoiceStatus=Invoice::findorFail($data['invoice_id']);
            $invoiceStatus->update([
                 'invoice_status'=>2,
            ]);
            Diagnose::create([
                'patient_id'=>$data['patient_id'],
                'doctor_id'=>$data['doctor_id'],
                'invoice_id'=>$data['invoice_id'],
                'diagnosis'=>$data['diagnosis'],
                'medicine'=>$data['medicine'],
                'date' => Carbon::now(),
                'review_date' => $data['review_date'],

            ]);
            DB::commit();
            return redirect()->route('Invoices.index');
        }
        catch(\Exception $e)
        {
            DB::rollBack();
            throw $e;
        }
    }

    public function show($id)
    {
        return view('Dashboard.doctor.Invoices.patient_record');
    }
}
