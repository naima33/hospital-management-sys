<?php

namespace App\Repository\DoctorDashboard;

use App\Interfaces\DoctorDashboard\InvoiceRepositoryInterface;
use App\Models\Invoice;
use App\Models\Laboratory;
use App\Models\Ray;
use Illuminate\Support\Facades\Auth;

class InvoiceRepository implements InvoiceRepositoryInterface
{
    public function index()
    {
        $invoices=Invoice::where('doctor_id',Auth::guard('doctor')->user()->id)->where('invoice_status',1)->get();
        return view('dashboard.doctor.Invoices.index',compact('invoices'));
    }

    public function reviewInvoices()
    {
        $invoices=Invoice::where('doctor_id',Auth::guard('doctor')->user()->id)->where('invoice_status',2)->get();
        return view('dashboard.doctor.Invoices.review-index',compact('invoices'));
    }

    public function completedInvoices()
    {
        $invoices=Invoice::where('doctor_id',Auth::guard('doctor')->user()->id)->where('invoice_status',3  )->get();
        return view('dashboard.doctor.Invoices.completed-index',compact('invoices'));
    }


    public function show($id)
    {
        $rays=Ray::findOrFail($id);
         return view('dashboard.doctor.Invoices.view_rays',compact('rays'));
    }


}
