<?php

namespace App\Repository\Dashboard_Ray_Employee;

use App\Interfaces\Dashboard_Ray_Employee\InvoiceRepositoryInterface;
use App\Models\Invoice;
use App\Models\Laboratory;
use App\Models\Ray;
use App\Traits\ImageTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Throwable;
use Illuminate\Support\Facades\Storage;

class InvoiceRepository implements InvoiceRepositoryInterface
{
    use ImageTrait;
    public function AllInvoices()
    {
        $invoices=Ray::all();
        return view('dashboard.RayEmployee.Invoices',compact('invoices'));
    }

    public function index()
    {
        $invoices=Ray::where('case',0)->get();
        return view('dashboard.Dashboard_Ray_Employee.Invoices.index',compact('invoices'));
    }

    public function completedInvoices()
    {
        $invoices=Ray::where('case',1)->get();
        return view('dashboard.Dashboard_Ray_Employee.Invoices.completedInvoices',compact('invoices'));
    }

    public function viewRays($id)
    {
        $rays=Ray::findOrFail($id);
         return view('dashboard.Dashboard_Ray_Employee.Invoices.view_rays',compact('rays'));
    }



    public function update($data,$id)
    {
        DB::beginTransaction();
        try{
            $Ray=Ray::findOrFail($id);
            $Ray->update([
                'employee_description'=>$data->employee_description,
                'employee_id'=>Auth()->user()->id,
                'case'=>1,
            ]);
            $this->UploadImage($data,'photos','rays','upload_image',$Ray->id,'App\Models\Ray',$Ray->patient->full_name);
            DB::commit();
            return redirect()->route('RayEmployeeInvoices.index');
        }
        catch(Throwable $e)
        {
            DB::rollBack();
            throw $e;
        }

    }

    public function destroy($id){
        $Ray=Ray::findOrFail($id);
        foreach($Ray->images as $image)
        {
            $img[]='rays/'.$image->filename;
        }
        Storage::disk('upload_image')->delete($img);
        $Ray->images()->delete();
        $Ray->delete();
        return redirect()->route('RayEmployeeInvoices.index');
    }


}
