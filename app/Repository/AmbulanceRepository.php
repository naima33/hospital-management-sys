<?php

namespace App\Repository;

use App\Interfaces\Ambulances\AmbulanceRepositoryInterface;
use App\Models\Ambulance;

class AmbulanceRepository implements AmbulanceRepositoryInterface
{
    public function index()
    {
        $Ambulances = Ambulance::all();
        return view('dashboard.Ambulances.index', compact('Ambulances'));
    }

    public function store($data)
    {
        Ambulance::create([
            'car_number' => $data['car_number'],
            'car_model' => $data['car_model'],
            'car_type' => $data['car_type'],
            'driver_name' => $data['driver_name'],
            'create_year' => $data['create_year'],
            'driver_license_number' => $data['license_number'],
            'driver_phone_number' => $data['driver_number'],
            'notes' => $data['notes'],

        ]);
        return redirect()->route('Ambulances.index');
    }

    public function create()
    {
        return view('dashboard.Ambulances.add');
    }

    public function edit($id)
    {
        $Ambulance = Ambulance::findOrFail($id);
        return view('dashboard.Ambulances.edit',compact('Ambulance'));
    }

    public function update($data, $id)
    {
        $Ambulance = Ambulance::findOrFail($id);
        $Ambulance->update([
            'car_number' => $data['car_number'],
            'car_model' => $data['car_model'],
            'car_type' => $data['car_type'],
            'car_type' => $data['car_type'],
            'create_year' => $data['create_year'],
            'driver_license_number' => $data['license_number'],
            'driver_phone_number' => $data['driver_number'],
            'notes' => $data['notes'],
        ]);
        return redirect()->route('Ambulances.index');
    }

    public function destroy($id)
    {
        $Ambulance = Ambulance::findOrFail($id);
        $Ambulance->delete();
        return redirect()->route('Ambulances.index');
    }

}
