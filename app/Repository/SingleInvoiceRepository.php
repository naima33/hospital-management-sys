<?php

namespace App\Repository;

use App\Interfaces\Invoices\SingleInvoiceRepositoryInterface;
use App\Models\Doctor;
use App\Models\FundAccount;
use App\Models\Group;
use App\Models\Invoice;
use App\Models\Patient;
use App\Models\PatientAccount;
use App\Models\Section;
use App\Models\SingleService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Throwable;

class SingleInvoiceRepository implements SingleInvoiceRepositoryInterface
{
    public function index()
    {
        $SingleInvoices = Invoice::all();
        return view('dashboard.Invoices.SingleInvoices.index', compact('SingleInvoices'));
    }

    public function create()
    {
        $patients = Patient::all();
        $doctors = Doctor::with('section')->get();
        $services = SingleService::all();
        $groups = Group::all();
        return view('dashboard.Invoices.SingleInvoices.add',compact('patients','doctors','services','groups'));
    }

    public function store($data)
    {
        DB::beginTransaction();
        try {

            if ($data['type'] == '2') {
                if ($data['section_id'] != null){
                    $section_id= Section::where('name',$data['section_id'])->first();
                }
                $group_id=null;
                if (array_key_exists('group_id',$data)){
                    $group_id=$data['group_id'];
                }
                $service_id=null;
                if (array_key_exists('service_id',$data)){
                    $service_id=$data['service_id'];
                }
                $invoice = Invoice::create([
                    'invoice_date' => $data['invoice_date'],
                    'invoice_type' => $data['invoice_type'],
                    'patient_id' => $data['patient_id'],
                    'doctor_id' => $data['doctor_id'],
                    'section_id' => $section_id->id,
                    'service_id' => $service_id,
                    'group_id' => $group_id,

                    'price' => $data['price'],
                    'discount_value' => $data['discount_value'],
                    'tax_rate' => $data['tax_rate'],
                    'tax_value' => $data['tax_value'],

                    'total_with_rate' => $total = $data['total'],
                    'type' => $data['type'],
                    'invoice_status' => 1,

                ]);
                FundAccount::create([
                    'date' => Carbon::now(),
                    'invoice_id' => $invoice->id,
                    'debit' => $total,
                    'credit' => 0.00,
                ]);
                DB::commit();
                return redirect()->route('SingleInvoices.index');
            } else {
                if ($data['section_id'] != null){
                    $section_id= Section::where('name',$data['section_id'])->first();
                }
                $group_id=null;
                if (array_key_exists('group_id',$data)){
                    $group_id=$data['group_id'];
                }
                $service_id=null;
                if (array_key_exists('service_id',$data)){
                    $service_id=$data['service_id'];
                }
                $invoice = Invoice::create([
                    'invoice_date' => $data['invoice_date'],
                    'invoice_type' => $data['invoice_type'],
                    'patient_id' => $data['patient_id'],
                    'doctor_id' => $data['doctor_id'],
                    'section_id' => $section_id->id,
                    'service_id' => $service_id,
                    'group_id' => $group_id,

                    'price' => $data['price'],
                    'discount_value' => $data['discount_value'],
                    'tax_rate' => $data['tax_rate'],
                    'tax_value' => $data['tax_value'],

                    'total_with_rate' => $total = $data['total'],
                    'type' => $data['type'],
                    'invoice_status' => 1,

                ]);
                PatientAccount::create([
                    'date' => Carbon::now(),
                    'invoice_id' => $invoice->id,
                    'patient_id' => $data['patient_id'],
                    'debit' => $total,
                    'credit' => 0.00,
                ]);
                DB::commit();
                return redirect()->route('SingleInvoices.index');
            }
        } catch (Throwable $e) {
            DB::rollback();
            throw $e;
        }

    }

    public function edit($id)
    {
        $Invoice = Invoice::with('patient','doctor','service','section')->findOrFail($id);
        $patients = Patient::all();
        $doctors = Doctor::with('section')->get();
        $services = SingleService::all();
        return view('dashboard.Invoices.SingleInvoices.edit',compact('Invoice','patients','doctors','services'));
    }

    public function update($data, $id)
    {
        DB::beginTransaction();
        try {

            if ($data['type'] == '2') {
                $Invoice = Invoice::findOrFail($id);
                if ($data['section_id'] != null){
                    $section_id= Section::where('name',$data['section_id'])->first();
                }
                $Invoice->update([
                        'invoice_date' => $data['invoice_date'],
                        'invoice_type' => $data['invoice_type'],
                        'patient_id' => $data['patient_id'],
                        'doctor_id' => $data['doctor_id'],
                        'section_id' => $section_id->id,
                        'service_id' => $data['service_id'],

                        'price' => $data['price'],
                        'discount_value' => $data['discount_value'],
                        'tax_rate' => $data['tax_rate'],
                        'tax_value' => $data['tax_value'],

                        'total_with_rate' => $total = $data['total'],
                        'type' => $data['type'],
                        'invoice_status' => 1,


                ]);
                $FundAccount = FundAccount::where('invoice_id',$id)->first();
                $FundAccount->update([
                        'date' => Carbon::now(),
                        'invoice_id' => $id,
                        'debit' => $total,
                        'credit' => 0.00,
                ]);
                DB::commit();
                return redirect()->route('SingleInvoices.index');
            } else {
                $Invoice = Invoice::findOrFail($id);
                if ($data['section_id'] != null){
                    $section_id= Section::where('name',$data['section_id'])->first();
                }
                $Invoice->update([
                    'invoice_date' => $data['invoice_date'],
                    'invoice_type' => $data['invoice_type'],
                    'patient_id' => $data['patient_id'],
                    'doctor_id' => $data['doctor_id'],
                    'section_id' => $section_id->id,
                    'service_id' => $data['service_id'],

                    'price' => $data['price'],
                    'discount_value' => $data['discount_value'],
                    'tax_rate' => $data['tax_rate'],
                    'tax_value' => $data['tax_value'],

                    'total_with_rate' => $total = $data['total'],
                    'type' => $data['type'],
                    'invoice_status' => 1,


            ]);
                $PatientAccount = PatientAccount::where('invoice_id',$id)->first();
                $PatientAccount->update([
                        'date' => Carbon::now(),
                        'invoice_id' => $id,
                        'patient_id' => $data['patient_id'],
                        'debit' => $total,
                        'credit' => 0.00,
                ]);
                DB::commit();
                return redirect()->route('SingleInvoices.index');
            }
        } catch (Throwable $e) {
            DB::rollback();
            throw $e;
        }

    }

    public function destroy($id)
    {
        $Patient = Invoice::findOrFail($id);
        $Patient->delete();
        return redirect()->route('SingleInvoices.index');
    }
}
