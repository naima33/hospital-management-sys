<?php

namespace App\Repository\Dashboard_Laboratorie_Employee;

use App\Interfaces\Dashboard_Laboratorie_Employee\InvoiceRepositoryInterface;
use App\Models\Invoice;
use App\Models\Laboratory;
use Illuminate\Support\Facades\Auth;
use App\Traits\ImageTrait;
use Illuminate\Support\Facades\DB;
use Throwable;
use Illuminate\Support\Facades\Storage;

class InvoiceRepository implements InvoiceRepositoryInterface
{
    use ImageTrait;
    public function AllInvoices()
    {
        $invoices=Laboratory::all();
        return view('dashboard.LaboratoryEmployee.Invoices',compact('invoices'));
    }

    public function index()
    {
        $invoices=Laboratory::where('case',0)->get();
        return view('dashboard.Dashboard_Laboratory_Employee.Invoices.index',compact('invoices'));
    }

    public function completedInvoices()
    {
        $invoices=Laboratory::where('case',1)->get();
        return view('dashboard.Dashboard_Laboratory_Employee.Invoices.completedInvoices',compact('invoices'));
    }

    public function viewLabs($id)
    {
        $Laboratories=Laboratory::findOrFail($id);
         return view('dashboard.Dashboard_Laboratory_Employee.Invoices.view_labs',compact('Laboratories'));
    }


    public function update($data,$id)
    {

        DB::beginTransaction();
        try{
        $laboratory=Laboratory::findOrFail($id);
        $laboratory->update([
            'employee_description'=>$data->employee_description,
            'employee_id'=>Auth()->user()->id,
            'case'=>1,
        ]);
         $this->UploadImage($data,'photos','labs','upload_image',$laboratory->id,'App\Models\Laboratory',$laboratory->patient->full_name);
        DB::commit();
        return redirect()->route('LabEmployeeInvoices.index');
    }
    catch(Throwable $e)
    {
        DB::rollBack();
        throw $e;
    }
    }

    public function destroy($id){
        $laboratory=Laboratory::findOrFail($id);
        foreach($laboratory->images as $image)
        {
            $img[]='labs/'.$image->filename;
        }
        Storage::disk('upload_image')->delete($img);
        $laboratory->images()->delete();
        $laboratory->delete();
        return redirect()->route('LabEmployeeInvoices.index');
    }


}
