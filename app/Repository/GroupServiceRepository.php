<?php

namespace App\Repository;

use App\Interfaces\Services\GroupServiceRepositoryInterface;
use App\Models\Group;
use App\Models\SingleService;
use Illuminate\Support\Facades\DB;
use Throwable;

class GroupServiceRepository implements GroupServiceRepositoryInterface
{
    public function index()
    {
        $GroupServices = Group::all();
        return view('dashboard.Services.group_services.index', compact('GroupServices'));
    }

    public function create()
    {
        $Services = SingleService::all();
        return view('dashboard.Services.group_services.add', compact('Services'));
    }

    public function store($data)
    {
        DB::beginTransaction();
        try {
            $Group = Group::create([
                'name' => $data['name'],
                'notes' => $data['notes'],
                'total_before_discount' => $data['total_before_discount'],
                'discount_value' => $data['discount_value'],
                'total_after_discount' => $data['total_before_discount'] - (int) $data['discount_value'],
                'tax_rate' => $data['tax_rate'],
                'total_with_tax' => $data['total_with_tax'],
            ]);
            $Group->services()->attach($data['service']);
            DB::commit();
            return redirect()->route('GroupServices.index');

        } catch (Throwable $e) {
            DB::rollback();
            throw $e;
        }

    }

    public function update($data, $id)
    {
        $Group = Group::findOrFail($id);
        $Group->update([
            'name' => $data['name'],
            'notes' => $data['notes'],
            'total_before_discount' => $data['total_before_discount'],
            'discount_value' => $data['discount_value'],
            'total_after_discount' => $subTotal = (int) $data['total_before_discount'] - (int) $data['discount_value'],
            'tax_rate' => $data['tax_rate'],
            'total_with_tax' => (((int) $data['tax_rate'] / 100) * $subTotal) + $subTotal,
        ]);
        $Group->service_group->attach($data->group_services);
        return redirect()->route('GroupServices.index');
    }

    public function destroy($id)
    {
        $SingleService = Group::findOrFail($id);
        $SingleService->delete();
        return redirect()->route('GroupServices.index');
    }

}
