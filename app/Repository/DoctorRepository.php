<?php

namespace App\Repository;

use App\Interfaces\Doctors\DoctorRepositoryInterface;
use App\Models\Appointment;
use App\Models\Doctor;
use App\Models\Section;
use App\Traits\ImageTrait;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Throwable;

class DoctorRepository implements DoctorRepositoryInterface
{
    use ImageTrait;
    public function index()
    {
        $doctors = Doctor::all();
        return view('dashboard.Doctors.index', compact('doctors'));
    }

    public function store($data)
    {
        $app = json_encode($data->appointments);
        DB::beginTransaction();
        try {
            $doctor = Doctor::create([
                'full_name' => $data->full_name,
                'email' => $data->email,
                'password' => Hash::make($data->password),
                'price' => $data->price,
                'phone' => $data->phone,
                'section_id' => $data->section_id,
                'status' => 1,
                'appointments' => $app,
            ]);
            $this->UploadProfileImage($data, 'photo', 'doctors', 'upload_image', $doctor->id, 'App\Models\Doctor');
            DB::commit();
            return redirect()->route('Doctors.index');
        } catch (Throwable $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function update($data, $id)
    {

        DB::beginTransaction();
        try {
            $Doctor = Doctor::findOrFail($id);
            if ($data->password == null) {
                $data = Arr::except($data, 'password');
            } else {
                $data['password'] = Hash::make($data['password']);
            }

            if ($data['status'] === 'on') {
                $data['status'] = 1;
            } else {
                $data['status'] = 0;
            }
            $app = json_encode($data->appointments);
            $data->appointments = $app;

            $Doctor->update($data->input());
            $this->UploadProfileImage($data, 'photo', 'doctors', 'upload_image', $id, 'App\Models\Doctor');
            DB::commit();
            return redirect()->route('Doctors.index');
        } catch (Throwable $e) {
            DB::rollBack();
            throw $e;
        }

        return redirect()->route('Doctors.index');
    }

    public function doctorActive($data, $id)
    {
        try {
            $Doctor = Doctor::findOrFail($id);
            $Doctor->update([
                'status' => $data['status'] === 'on' ? 1 : 0,
            ]);
            return redirect()->route('Doctors.index');
        } catch (Throwable $e) {

            throw $e;
        }
    }

    public function destroy($id)
    {
        $Doctor = Doctor::findOrFail($id);
        $Doctor->delete();
        return redirect()->route('Doctors.index');
    }

    public function edit($id)
    {
        $sections = Section::all();
        $appointment = Appointment::all();
        $doctor = Doctor::findOrfail($id);
        return view('dashboard.Doctors.edit', compact('sections', 'appointment', 'doctor'));
    }
}
