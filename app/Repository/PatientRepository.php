<?php

namespace App\Repository;

use App\Interfaces\Patients\PatientRepositoryInterface;
use App\Models\Invoice;
use App\Models\Laboratory;
use App\Models\Patient;
use App\Models\PatientAccount;
use App\Models\Payment;
use App\Models\Ray;
use App\Models\Receipt;
use Illuminate\Support\Facades\Hash;

class PatientRepository implements PatientRepositoryInterface
{
    public function index()
    {
        $Patients = Patient::all();
        return view('dashboard.Patients.index', compact('Patients'));
    }

    public function create()
    {
        return view('dashboard.Patients.add');
    }

    public function store($data)
    {
        Patient::create([
            'full_name' => $data['full_name'],
            'email' => $data['email'],
            'birth_date' => $data['birth_date'],
            'phone' => $data['phone'],
            'gender' => $data['gender'],
            'address' => $data['address'],
            'blood_group' => $data['blood_type'],
            'password' => Hash::make($data['phone']) ,

        ]);
        return redirect()->route('Patients.index');
    }

    public function edit($id)
    {
        $Patient = Patient::findOrFail($id);
        return view('dashboard.Patients.edit',compact('Patient'));
    }

    public function update($data, $id)
    {
        $Patient = Patient::findOrFail($id);
        $Patient->update([
            'full_name' => $data['full_name'],
            'email' => $data['email'],
            'birth_date' => $data['birth_date'],
            'phone' => $data['phone'],
            'gender' => $data['gender'],
            'address' => $data['address'],
            'blood_type' => $data['blood_type'],
            'password' => Hash::make($data['phone']) ,
        ]);
        return redirect()->route('Patients.index');
    }

    public function destroy($id)
    {
        $Patient = Patient::findOrFail($id);
        $Patient->delete();
        return redirect()->route('Patients.index');
    }

    public function show($id){
        $patient=Patient::findOrFail($id);
        $invoices=Invoice::where('patient_id',$id)->with('service')->get();
        $receipts=Receipt::where('patient_id',$id)->get();
        $PatientAccounts=PatientAccount::orWhereNotNull('invoice_id')->
        orWhereNotNull('receipt_id')->
        orWhereNotNull('payment_id')->
        where('patient_id',$id)->get();
        $ray=Ray::where('patient_id',$id)->get();
        $laboratory=Laboratory::where('patient_id',$id)->get();
        return view('dashboard.Patients.show',compact('patient','invoices','receipts','PatientAccounts','ray','laboratory'));
    }
}
