<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SingleService extends Model
{
    use HasFactory;
    protected $fillable=[
        'name',
        'price',
        'status',
        'description'
     ];

     public function patient(): BelongsTo
     {
         return $this->belongsTo(Patient::class);
     }

     public function doctor(): BelongsTo
     {
         return $this->belongsTo(Doctor::class);
     }

     public function section(): BelongsTo
     {
         return $this->belongsTo(Section::class);
     }

     public function service(): BelongsTo
     {
         return $this->belongsTo(SingleService::class);
     }

}
