<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Diagnose extends Model
{
    use HasFactory;

    protected $fillable=[
        'diagnose',
        'description',
        'patient_id',
        'doctor_id',
        'invoice_id',
        'review_date',
        'date'
     ];

    public function doctor(): BelongsTo
    {
        return $this->belongsTo(Doctor::class,'doctor_id');
    }
}
