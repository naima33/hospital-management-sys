<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Invoice extends Model
{
    use HasFactory;
    protected $fillable=[
        'invoice_date',
        'invoice_type',
        'patient_id',
        'doctor_id',
        'section_id',
        'service_id',
        'group_id',
        'price',
        'discount_value',
        'tax_rate',
        'tax_value',
        'total_with_rate',
        'type',
        'invoice_status',
     ];

     public function group(): BelongsTo
    {
        return $this->belongsTo(Group::class);
    }

     public function patient(): BelongsTo
    {
        return $this->belongsTo(Patient::class);
    }

    public function doctor(): BelongsTo
    {
        return $this->belongsTo(Doctor::class);
    }

    public function section(): BelongsTo
    {
        return $this->belongsTo(Section::class);
    }

    public function service(): BelongsTo
    {
        return $this->belongsTo(SingleService::class);
    }

}
