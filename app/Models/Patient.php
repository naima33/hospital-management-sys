<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Patient extends Authenticatable
{
    use HasFactory;
    protected $fillable=[
        'full_name',
        'email',
        'birth_date',
        'password',
        'gender',
        'phone',
        'address',
        'blood_group',
     ];
}
