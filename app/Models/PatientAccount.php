<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PatientAccount extends Model
{
    use HasFactory;
    protected $fillable=[
        'date',
        'invoice_id',
        'patient_id',
        'payment_id',
        'debit',
        'credit',
     ];

     public function invoice(): belongsTo
    {
        return $this->belongsTo(Invoice::class);
    }

    public function receipts(): belongsTo
    {
        return $this->belongsTo(Receipt::class);
    }

    public function payments(): belongsTo
    {
        return $this->belongsTo(Payment::class);
    }
}
