<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assurance extends Model
{
    use HasFactory;
    protected $table='insurances';
    protected $fillable=[
        'notes',
        'name',
        'discount_percentage',
        'company_rate',
        'status',

     ];
}
