<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Ray extends Model
{
    use HasFactory;

    protected $fillable=[
        'description',
        'patient_id',
        'doctor_id',
        'invoice_id',
        'employee_id',
        'employee_description',
        'case'
    ];

    public function doctor(): BelongsTo
    {
        return $this->belongsTo(Doctor::class);
    }

    public function employee(): BelongsTo
    {
        return $this->belongsTo(RayEmployee::class,'employee_id');
    }

    public function patient(): BelongsTo
    {
        return $this->belongsTo(Patient::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
}
