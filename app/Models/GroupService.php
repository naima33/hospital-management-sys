<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupService extends Model
{
    use HasFactory;
    protected $table='group_services';
    protected $fillable=[
        'name',
        'notes',
        'before_discount',
        'discount_value',
        'total_after_discount',
        'tax_rate',
        'total_with_tax'
     ];
}
