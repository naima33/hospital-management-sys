<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ambulance extends Model
{
    use HasFactory;
    protected $fillable=[
        'car_number',
        'car_model',
        'car_type',
        'driver_name',
        'create_year',
        'driver_license_number',
        'driver_phone_number',
        'notes',
     ];
}
