<?php

namespace App\Traits;

use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

trait ImageTrait
{
    public function UploadProfileImage(Request $request,$inputName,$folderName,$disk,$imageable_id,$imageable_type)
    {
        if($request->hasFile($inputName))
        {
                {
                  if(!$request->file($inputName)->isValid())
                  {
                      return redirect()->back()->withInput();
                  }
                  $img=$request->file($inputName);
                  $name=Str::slug($request->full_name);
                  $filename=$name.time().'.'.$img->getClientOriginalExtension();

                  $Image=new Image();
                  $Image->filename=$filename;
                  $Image->imageable_id=$imageable_id;
                  $Image->imageable_type=$imageable_type;
                  $Image->save();
                  $request->file($inputName)->storeAs($folderName,$filename,$disk);
                }
            return 'saved';
        }

      return null;

    }
    public function UploadImage(Request $request,$inputName,$folderName,$disk,$imageable_id,$imageable_type,$name=null)
    {
        if($request->hasFile($inputName))
        {

            foreach($request->photos as $i=>$photo)
            {
                {
                  if(!$photo->isValid())
                  {
                      return redirect()->back()->withInput();
                  }
                  $img=$photo;
                  $name=Str::slug($name);
                  $filename=$name.time().$i.'.'.$img->getClientOriginalExtension();

                  $Image=new Image();
                  $Image->filename=$filename;
                  $Image->imageable_id=$imageable_id;
                  $Image->imageable_type=$imageable_type;
                  $Image->save();
                  $photo->storeAs($folderName,$filename,$disk);
                }
            }
            return 'saved';
        }

      return null;
    }

    public function DeleteImage(Request $request,$inputName,$disk)
    {

    }
}
