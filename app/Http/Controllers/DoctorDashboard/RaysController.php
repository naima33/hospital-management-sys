<?php

namespace App\Http\Controllers\DoctorDashboard;

use App\Http\Controllers\Controller;
use App\Interfaces\DoctorDashboard\RayRepositoryInterface;
use Illuminate\Http\Request;

class RaysController extends Controller
{

    private $Rays;

    public function __construct(RayRepositoryInterface $Rays)
    {
        $this->Rays=$Rays;
    }

    public function store(Request $request)
    {
        return $this->Rays->store($request->input());
    }

    public function update(Request $request, $id)
    {
        return $this->Rays->update($request->input(),$id);
    }

    public function destroy($id)
    {
        return $this->Rays->destroy($id);
    }
}
