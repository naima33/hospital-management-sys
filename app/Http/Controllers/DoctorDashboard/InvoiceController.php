<?php

namespace App\Http\Controllers\DoctorDashboard;

use App\Http\Controllers\Controller;
use App\Interfaces\DoctorDashboard\InvoiceRepositoryInterface;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    private $Invoice;

    public function __construct(InvoiceRepositoryInterface $Invoice)
    {
        $this->Invoice=$Invoice;
    }

    public function index()
    {
        return $this->Invoice->index();
    }

    public function reviewInvoices()
    {
        return $this->Invoice->reviewInvoices();
    }

    public function completedInvoices()
    {
        return $this->Invoice->completedInvoices();
    }

    public function show($id)
    {
        return $this->Invoice->show($id);
    }

}
