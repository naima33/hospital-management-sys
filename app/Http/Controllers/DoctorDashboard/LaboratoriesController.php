<?php

namespace App\Http\Controllers\DoctorDashboard;

use App\Http\Controllers\Controller;
use App\Interfaces\DoctorDashboard\LaboratoryRepositoryInterface;
use Illuminate\Http\Request;

class LaboratoriesController extends Controller
{
    private $Laboratory;

    public function __construct(LaboratoryRepositoryInterface $Laboratory)
    {
        $this->Laboratory=$Laboratory;
    }

    public function update(Request $request, $id)
    {
        return $this->Laboratory->update($request->input(), $id);
    }

    public function destroy($id)
    {
        return $this->Laboratory->destroy($id);
    }
}
