<?php

namespace App\Http\Controllers\DoctorDashboard;

use App\Http\Controllers\Controller;
use App\Interfaces\DoctorDashboard\DiagnosisRepositoryInterface;
use Illuminate\Http\Request;

class DiagnosisController extends Controller
{
    private $Diagnosis;

    public function __construct(DiagnosisRepositoryInterface $Diagnosis)
    {
        $this->Diagnosis=$Diagnosis;
    }

    public function store(Request $request)
    {
        return $this->Diagnosis->store($request->input());
    }

    public function add_review(Request $request)
    {
        return $this->Diagnosis->add_review($request->input());
    }

    public function show($id)
    {
        return $this->Diagnosis->show($id);
    }

}
