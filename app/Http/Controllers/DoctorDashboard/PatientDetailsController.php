<?php

namespace App\Http\Controllers\DoctorDashboard;

use App\Http\Controllers\Controller;
use App\Models\Diagnose;
use App\Models\Laboratory;
use App\Models\Ray;
use Illuminate\Http\Request;

class PatientDetailsController extends Controller
{

    public function index($id)
    {
        $patient_records=Diagnose::where('patient_id',$id)->get();
        $patient_rays=Ray::where('patient_id',$id)->get();
        $patient_labs=Laboratory::where('patient_id',$id)->get();
        return view('Dashboard.doctor.Invoices.patient_details',compact('patient_records','patient_rays','patient_labs'));
    }

}
