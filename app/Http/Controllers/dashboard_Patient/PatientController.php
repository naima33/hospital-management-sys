<?php

namespace App\Http\Controllers\Dashboard_Patient;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Laboratory;
use App\Models\Ray;
use Illuminate\Http\Request;

class PatientController extends Controller
{

    public function Invoices()
    {
        $invoices=Invoice::where('patient_id',auth()->user()->id)->get();
        return view('dashboard.Dashboard_Patient.invoices',compact('invoices'));
    }

    public function Laboratories()
    {
        $labs=Laboratory::where('patient_id',auth()->user()->id)->get();
        return view('dashboard.Dashboard_Patient.laboratories',compact('labs'));
    }

    public function Rays()
    {
        $rays=Ray::where('patient_id',auth()->user()->id)->get();
        return view('dashboard.Dashboard_Patient.rays',compact('rays'));
    }

}
