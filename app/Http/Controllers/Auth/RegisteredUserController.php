<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\View\View;
use Illuminate\Support\Facades\DB;
use Throwable;
use App\Traits\ImageTrait;

class RegisteredUserController extends Controller
{
    use ImageTrait;
    /**
     * Display the registration view.
     */
    public function create(): View
    {
        return view('auth.register1');

    }

    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {

            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:'.Admin::class],
                'password' => ['required', 'confirmed', Rules\Password::defaults()],
            ]);

            $admin = Admin::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);


            event(new Registered($admin));

            Auth::guard('admin')->login($admin);

            $this->UploadProfileImage($request, 'photo', 'admins', 'upload_image', $admin->id, 'App\Models\Admin');

            DB::commit();
            return redirect(RouteServiceProvider::ADMIN);
        } catch (Throwable $e) {
            DB::rollBack();
            throw $e;
        }

    }
}
