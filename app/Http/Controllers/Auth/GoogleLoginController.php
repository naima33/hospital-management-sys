<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Doctor;
use App\Models\Laboratory;
use App\Models\Patient;
use App\Models\RayEmployee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class GoogleLoginController extends Controller
{
    public function redirectToGoogle()
    {

        return Socialite::driver('google')->redirect();
    }


    public function handleGoogleCallback()
    {
        // return $type;
        // $googleUser = Socialite::driver('google')->stateless()->user();
        // if($type==='admin')
        // $user=Admin::where('email', $googleUser->email)->first();
        // elseif($type==='doctor')
        // $user=Doctor::where('email', $googleUser->email)->first();
        // elseif($type==='rayEmployee')
        // $user=RayEmployee::where('email', $googleUser->email)->first();
        // elseif($type==='labEmployee')
        // $user=Laboratory::where('email', $googleUser->email)->first();
        // elseif($type==='patient')
        // $user=Patient::where('email', $googleUser->email)->first();
        $googleUser = Socialite::driver('google')->stateless()->user();
        $user = Admin::where('email', $googleUser->email)->first();
        if(!$user)
        {
            $user = admin::create(['name' => $googleUser->name, 'email' => $googleUser->email, 'password' => Hash::make(123456789)]);
        }

        Auth::login($user);

        return redirect(RouteServiceProvider::ADMIN);
    }
}
