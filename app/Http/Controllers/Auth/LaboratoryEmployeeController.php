<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\laboratoryEmployeeLoginRequest;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class LaboratoryEmployeeController extends Controller
{

    public function create()
    {

        return view('dashboard.user.auth.loginPage');
    }


    public function store(laboratoryEmployeeLoginRequest $request): RedirectResponse
    {

            $request->authenticate();

            $request->session()->regenerate();

            return redirect()->intended(RouteServiceProvider::LaboratoryEmployee);


    }

    public function destroy(Request $request): RedirectResponse
    {
        Auth::guard('laboratoryEmployee')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
