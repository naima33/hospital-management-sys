<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Auth\AdminLoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    public function create()
    {
        return view('dashboard.user.auth.loginPage');
    }

    public function store(AdminLoginRequest $request)
    {

            $request->authenticate();

             $request->session()->regenerate();

            return redirect()->intended(RouteServiceProvider::ADMIN);

    }

    public function destroy(Request $request): RedirectResponse
    {

        Auth::guard('admin')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
