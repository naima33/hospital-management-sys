<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;

use App\Interfaces\Invoices\SingleInvoiceRepositoryInterface;
use Illuminate\Http\Request;

class SingleInvoiceController extends Controller
{
    private $SingleInvoice;

    public function __construct(SingleInvoiceRepositoryInterface $SingleInvoice)
    {
        $this->SingleInvoice=$SingleInvoice;
    }

    public function index()
    {
        return $this->SingleInvoice->index();
    }

    public function create()
    {
        return $this->SingleInvoice->create();
    }


    public function store(Request $request)
    {
        return $this->SingleInvoice->store($request->input());
    }

    public function edit($id)
    {
        return $this->SingleInvoice->edit($id);
    }

    public function update(Request $request, $id)
    {
        return $this->SingleInvoice->update($request->input(), $id);
    }

    public function destroy($id)
    {
        return $this->SingleInvoice->destroy($id);
    }
}
