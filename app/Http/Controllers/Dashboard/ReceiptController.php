<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Interfaces\Finances\ReceiptRepositoryInterface;
use Illuminate\Http\Request;

class ReceiptController extends Controller
{
    private $Receipts;

    public function __construct(ReceiptRepositoryInterface $Receipts)
    {
        $this->Receipts=$Receipts;
    }

    public function index()
    {
        return $this->Receipts->index();
    }

    public function create()
    {
        return $this->Receipts->create();
    }

    public function store(Request $request)
    {
        return $this->Receipts->store($request->input());
    }

    public function show($id)
    {
        return $this->Receipts->show($id);
    }

    public function edit($id)
    {
        return $this->Receipts->edit($id);
    }

    public function update(Request $request, $id)
    {
        return $this->Receipts->update($request->input(), $id);
    }

    public function destroy($id)
    {
        return $this->Receipts->destroy($id);
    }

    public function PrintBill(Request $request,$id)
    {
        return $this->Receipts->PrintBill($id);
    }
}
