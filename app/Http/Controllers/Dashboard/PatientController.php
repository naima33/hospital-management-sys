<?php

namespace App\Http\Controllers\Dashboard;

use App\Interfaces\Patients\PatientRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    private $Patient;

    public function __construct(PatientRepositoryInterface $Patient)
    {
        $this->Patient=$Patient;
    }

    public function index()
    {
        return $this->Patient->index();
    }

    public function create()
    {
        return $this->Patient->create();
    }

    public function store(Request $request)
    {
        return $this->Patient->store($request->input());
    }

    public function show($id)
    {
        return $this->Patient->show($id);
    }

    public function edit($id)
    {
        return $this->Patient->edit($id);
    }

    public function update(Request $request, $id)
    {
        return $this->Patient->update( $request->input(), $id);
    }

    public function destroy($id)
    {
        return $this->Patient->destroy($id);
    }
}
