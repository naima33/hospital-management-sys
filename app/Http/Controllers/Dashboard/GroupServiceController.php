<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Interfaces\Services\GroupServiceRepositoryInterface;
use Illuminate\Http\Request;

class GroupServiceController extends Controller
{
    private $GroupService;

    public function __construct(GroupServiceRepositoryInterface $GroupService)
    {
        $this->GroupService=$GroupService;
    }

    public function index()
    {
        return $this->GroupService->index();
    }

    public function create()
    {
        return $this->GroupService->create();
    }

    public function store(Request $request)
    {
        return $this->GroupService->store($request->input());
    }


    public function update(Request $request, $id)
    {
        return $this->GroupService->update($request->input(), $id);
    }

    public function destroy($id)
    {
        return $this->GroupService->destroy($id);
    }
}
