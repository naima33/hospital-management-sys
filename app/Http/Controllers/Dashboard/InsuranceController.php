<?php

namespace App\Http\Controllers\Dashboard;

use App\Interfaces\Insurances\InsuranceRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InsuranceController extends Controller
{
    private $Insurance;

    public function __construct(InsuranceRepositoryInterface $Insurance)
    {
        $this->Insurance=$Insurance;
    }

    public function index()
    {
        return $this->Insurance->index();
    }

    public function create()
    {
        return $this->Insurance->create();
    }

    public function store(Request $request)
    {
        return $this->Insurance->store($request->input());
    }

    public function insuranceActive(Request $request, $id)
    {
        return $this->Insurance->insuranceActive($request->input(),$id);
    }

    public function update(Request $request, $id)
    {
        return $this->Insurance->update($request->input(), $id);
    }

    public function destroy($id)
    {
        return $this->Insurance->destroy($id);
    }
}
