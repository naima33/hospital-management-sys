<?php

namespace App\Http\Controllers\Dashboard;

use App\Interfaces\Ambulances\AmbulanceRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AmbulanceController extends Controller
{
    private $Ambulance;

    public function __construct(AmbulanceRepositoryInterface $Ambulance)
    {
        $this->Ambulance=$Ambulance;
    }

    public function index()
    {
        return $this->Ambulance->index();
    }

    public function create()
    {
        return $this->Ambulance->create();
    }

    public function store(Request $request)
    {
        return $this->Ambulance->store($request->input());
    }

    public function edit($id)
    {
        return $this->Ambulance->edit($id);
    }

    public function update(Request $request, $id)
    {
        return $this->Ambulance->update($request->input(), $id);
    }

    public function destroy($id)
    {
        return $this->Ambulance->destroy($id);
    }
}
