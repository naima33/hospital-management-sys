<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Interfaces\Finances\PaymentRepositoryInterface;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    private $Payments;

    public function __construct(PaymentRepositoryInterface $Payments)
    {
        $this->Payments=$Payments;
    }

    public function index()
    {
        return $this->Payments->index();
    }

    public function create()
    {
        return $this->Payments->create();
    }

    public function store(Request $request)
    {
        return $this->Payments->store($request->input());
    }

    public function edit($id)
    {
        return $this->Payments->edit($id);
    }

    public function update(Request $request, $id)
    {
        return $this->Payments->update($request->input(), $id);
    }

    public function destroy($id)
    {
        return $this->Payments->destroy($id);
    }

    public function PrintBill($id)
    {
        return $this->Payments->PrintBill($id);
    }
}
