<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Interfaces\LaboratoryEmployee\LaboratoryEmployeeRepositoryInterface;
use Illuminate\Http\Request;

class LaboratoryEmployeeController extends Controller
{
    private $LaboratoryEmployee;

    public function __construct(LaboratoryEmployeeRepositoryInterface $LaboratoryEmployee)
    {
        $this->LaboratoryEmployee=$LaboratoryEmployee;
    }

    public function index()
    {
        return $this->LaboratoryEmployee->index();
    }

    public function store(Request $request)
    {
        return $this->LaboratoryEmployee->store($request->all());
    }

    public function update(Request $request, $id)
    {
        return $this->LaboratoryEmployee->update($request->all(), $id);
    }

    public function destroy($id)
    {
        return $this->LaboratoryEmployee->destroy($id);
    }
}
