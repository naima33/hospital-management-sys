<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Interfaces\RayEmployees\RayEmployeeRepositoryInterface;
use Illuminate\Http\Request;

class RayEmployeeController extends Controller
{
    private $RayEmployee;

    public function __construct(RayEmployeeRepositoryInterface $RayEmployee)
    {
        $this->RayEmployee=$RayEmployee;
    }

    public function index()
    {
        return $this->RayEmployee->index();
    }

    public function store(Request $request)
    {
        return $this->RayEmployee->store($request->input());
    }

    public function update(Request $request, $id)
    {
        return $this->RayEmployee->update($request->all(), $id);
    }

    public function destroy($id)
    {
        return $this->RayEmployee->destroy($id);
    }
}
