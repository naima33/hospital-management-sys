<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Interfaces\Sections\SectionRepositoryInterface;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    private $Sections;

    public function __construct(SectionRepositoryInterface $Sections)
    {
        $this->Sections=$Sections;
    }

    public function index()
    {
       return $this->Sections->index();
    }

    public function create()
    {
        return view('dashboard.Sections.add');
    }

    public function store(Request $request)
    {
        return $this->Sections->store($request->input());
    }

    public function show($id)
    {
        return $this->Sections->show($id);
    }

    public function edit($id)
    {
        return view('dashboard.Sections.edit',compact('id'));
    }

    public function update(Request $request, $id)
    {
        return $this->Sections->update($request->input(),$id);
    }

    public function destroy($id)
    {
        return $this->Sections->destroy($id);
    }

}
