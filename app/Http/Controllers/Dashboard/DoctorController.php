<?php

namespace App\Http\Controllers\Dashboard;

use App\Interfaces\Doctors\DoctorRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    private $Doctors;

    public function __construct(DoctorRepositoryInterface $Doctors)
    {
        $this->Doctors=$Doctors;
    }

    public function index()
    {
        return $this->Doctors->index();
    }

    public function store(Request $request)
    {
        return $this->Doctors->store($request);
    }


    public function edit($id)
    {
        return $this->Doctors->edit($id);
    }

    public function update(Request $request, $id)
    {
        return $this->Doctors->update($request,$id);
    }

    public function doctorActive(Request $request, $id)
    {
        return $this->Doctors->doctorActive($request->input(),$id);
    }

    public function destroy($id)
    {
        return $this->Doctors->destroy($id);
    }
}
