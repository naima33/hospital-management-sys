<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Interfaces\Services\SingleServiceRepositoryInterface;
use Illuminate\Http\Request;

class SingleServiceController extends Controller
{
    private $SingleService;

    public function __construct(SingleServiceRepositoryInterface $SingleService)
    {
        $this->SingleService=$SingleService;
    }

    public function index()
    {
        return $this->SingleService->index();
    }

    public function create()
    {
        return $this->SingleService->create();
    }

    public function store(Request $request)
    {
        return $this->SingleService->store($request->input());
    }

    public function update(Request $request, $id)
    {
        return $this->SingleService->update($request->input(), $id);
    }

    public function destroy($id)
    {
        return $this->SingleService->destroy($id);
    }
}
