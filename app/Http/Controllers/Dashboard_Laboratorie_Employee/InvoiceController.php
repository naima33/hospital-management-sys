<?php

namespace App\Http\Controllers\Dashboard_Laboratorie_Employee;

use App\Http\Controllers\Controller;
use App\Interfaces\Dashboard_Laboratorie_Employee\InvoiceRepositoryInterface;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    private $Invoice;

    public function __construct(InvoiceRepositoryInterface $Invoice)
    {
        $this->Invoice=$Invoice;
    }

     public function AllInvoices()
     {
         return $this->Invoice->AllInvoices();
     }

    public function index()
    {
        return $this->Invoice->index();
    }

    public function completedInvoices()
    {
        return $this->Invoice->completedInvoices();
    }

    public function viewLabs($id)
    {
        return $this->Invoice->viewLabs($id);
    }


    public function update(Request $request, $id)
    {
        return $this->Invoice->update($request, $id);
    }

    public function destroy($id)
    {
        return $this->Invoice->destroy( $id);
    }
}
