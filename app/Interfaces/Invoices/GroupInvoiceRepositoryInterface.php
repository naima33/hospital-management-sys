<?php

namespace App\Interfaces\Patients;

interface PatientRepositoryInterface
{
    public function index();

    public function store($data);

    public function update($data,$id);

    public function destroy($id);

}
