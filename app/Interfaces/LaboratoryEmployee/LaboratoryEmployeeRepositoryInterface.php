<?php

namespace App\Interfaces\LaboratoryEmployee;

interface LaboratoryEmployeeRepositoryInterface
{
    public function index();

    public function store($data);

    public function update($data,$id);

    public function destroy($id);

}
