<?php

namespace App\Interfaces\RayEmployees;

interface RayEmployeeRepositoryInterface
{
    public function index();

    public function store($data);

    public function update($data,$id);

    public function destroy($id);

}
