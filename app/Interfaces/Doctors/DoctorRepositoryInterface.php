<?php

namespace App\Interfaces\Doctors;
interface DoctorRepositoryInterface
{
    public function index();

    public function store($data);

    public function doctorActive($data,$id);

    public function edit( $id);

    public function update($data,$id);

    public function destroy($id);

}
