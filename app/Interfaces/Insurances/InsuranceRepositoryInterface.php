<?php

namespace App\Interfaces\Insurances;

interface InsuranceRepositoryInterface
{
    public function index();

    public function store($data);

    public function insuranceActive($data,$id);

    public function create();

    public function update($data,$id);

    public function destroy($id);

}
