<?php

namespace App\Interfaces\Ambulances;

interface AmbulanceRepositoryInterface
{
    public function index();

    public function store($data);

    public function create();

    public function edit( $id);

    public function update($data,$id);

    public function destroy($id);


}
