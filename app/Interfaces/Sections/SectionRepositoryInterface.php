<?php

namespace App\Interfaces\Sections;

interface SectionRepositoryInterface
{
    public function index();

    public function store($data);

    public function show($id);

    public function update($data,$id);

    public function destroy($id);

}
