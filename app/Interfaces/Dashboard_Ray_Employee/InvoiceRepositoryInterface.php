<?php

namespace App\Interfaces\Dashboard_Ray_Employee;

interface InvoiceRepositoryInterface
{
    public function AllInvoices();

    public function index();

    public function completedInvoices();

    public function viewRays($id);

    public function update($data,$id);

    public function destroy($id);

}
