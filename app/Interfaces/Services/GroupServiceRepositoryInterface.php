<?php

namespace App\Interfaces\Services;

interface GroupServiceRepositoryInterface
{
    public function index();

    public function store($data);

    public function create();

    public function update($data,$id);

    public function destroy($id);
}
