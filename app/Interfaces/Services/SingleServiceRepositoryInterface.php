<?php

namespace App\Interfaces\Services;

interface SingleServiceRepositoryInterface
{
    public function index();

    public function store($data);

    public function create();

    public function update($data,$id);

    public function destroy($id);

}
