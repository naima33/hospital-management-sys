<?php

namespace App\Interfaces\Dashboard_Laboratorie_Employee;

interface InvoiceRepositoryInterface
{
    public function AllInvoices();

    public function index();

    public function completedInvoices();

    public function viewLabs($id);

    public function update($data,$id);

    public function destroy($id);

}
