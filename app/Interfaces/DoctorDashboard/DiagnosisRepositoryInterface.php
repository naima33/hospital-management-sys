<?php

namespace App\Interfaces\DoctorDashboard;
interface DiagnosisRepositoryInterface
{
    public function store($data);

    public function show($id);

    public function add_review($data);

}
