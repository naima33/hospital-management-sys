<?php

namespace App\Interfaces\DoctorDashboard;

interface LaboratoryRepositoryInterface
{
    public function store($data);

    public function update($data,$id);

    public function destroy($id);

}
