<?php

namespace App\Interfaces\DoctorDashboard;

interface RayRepositoryInterface
{
    public function store($data);

    public function update($data,$id);

    public function destroy($id);

}
