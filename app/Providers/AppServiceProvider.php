<?php

namespace App\Providers;

use App\Interfaces\Ambulances\AmbulanceRepositoryInterface;
use App\Interfaces\DoctorDashboard\DiagnosisRepositoryInterface;
use App\Interfaces\DoctorDashboard\InvoiceRepositoryInterface;
use Illuminate\Support\ServiceProvider;
use App\Interfaces\Sections\SectionRepositoryInterface;
use App\Repository\SectionRepository;
use App\Interfaces\Insurances\InsuranceRepositoryInterface;
use App\Interfaces\Doctors\DoctorRepositoryInterface;
use App\Interfaces\Finances\PaymentRepositoryInterface;
use App\Interfaces\Finances\ReceiptRepositoryInterface;
use App\Interfaces\Invoices\SingleInvoiceRepositoryInterface;
use App\Interfaces\DoctorDashboard\LaboratoryRepositoryInterface;
use App\Interfaces\Patients\PatientRepositoryInterface;
use App\Interfaces\DoctorDashboard\RayRepositoryInterface;
use App\Interfaces\LaboratoryEmployee\LaboratoryEmployeeRepositoryInterface;
use App\Interfaces\RayEmployees\RayEmployeeRepositoryInterface;
use App\Interfaces\Services\GroupServiceRepositoryInterface;
use App\Interfaces\Services\SingleServiceRepositoryInterface;
use App\Repository\AmbulanceRepository;
use App\Repository\DoctorDashboard\DiagnoseRepository;
use App\Repository\DoctorDashboard\InvoiceRepository;
use App\Repository\InsuranceRepository;
use App\Repository\DoctorRepository;
use App\Repository\PatientRepository;
use App\Repository\Finances\PaymentRepository;
use App\Repository\Finances\ReceiptRepository;
use App\Repository\GroupServiceRepository;
use App\Repository\DoctorDashboard\LaboratoryRepository;
use App\Repository\DoctorDashboard\RayRepository;
use App\Repository\LaboratoryEmployeeRepository;
use App\Repository\RayEmployeeRepository;
use App\Repository\SingleInvoiceRepository;
use App\Repository\SingleServiceRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //admin
        $this->app->bind(SectionRepositoryInterface::class,SectionRepository::class);
        $this->app->bind(DoctorRepositoryInterface::class,DoctorRepository::class);
        $this->app->bind(SingleServiceRepositoryInterface::class,SingleServiceRepository::class);
        $this->app->bind(GroupServiceRepositoryInterface::class,GroupServiceRepository::class);
        $this->app->bind(InsuranceRepositoryInterface::class,InsuranceRepository::class);
        $this->app->bind(AmbulanceRepositoryInterface::class,AmbulanceRepository::class);
        $this->app->bind(PatientRepositoryInterface::class,PatientRepository::class);
        $this->app->bind(SingleInvoiceRepositoryInterface::class,SingleInvoiceRepository::class);
        $this->app->bind(ReceiptRepositoryInterface::class,ReceiptRepository::class);
        $this->app->bind(PaymentRepositoryInterface::class,PaymentRepository::class);

        //doctor
        $this->app->bind(InvoiceRepositoryInterface::class,InvoiceRepository::class);
        $this->app->bind(DiagnosisRepositoryInterface::class,DiagnoseRepository::class);
        $this->app->bind(RayRepositoryInterface::class,RayRepository::class);
        $this->app->bind(LaboratoryRepositoryInterface::class,LaboratoryRepository::class);

        //ray Employee
        $this->app->bind(RayEmployeeRepositoryInterface::class,RayEmployeeRepository::class);
        $this->app->bind('App\Interfaces\Dashboard_Ray_Employee\InvoiceRepositoryInterface',
        'App\Repository\Dashboard_Ray_Employee\InvoiceRepository');

        //lab Employee
        $this->app->bind(LaboratoryEmployeeRepositoryInterface::class,
        LaboratoryEmployeeRepository::class);
        $this->app->bind('App\Interfaces\Dashboard_Laboratorie_Employee\InvoiceRepositoryInterface',
        'App\Repository\Dashboard_Laboratorie_Employee\InvoiceRepository');

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
