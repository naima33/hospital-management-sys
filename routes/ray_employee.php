<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dashboard_Ray_Employee\InvoiceController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



require __DIR__ . '/auth.php';



//dashboard
Route::get('/', function () {
    return view('welcome');
});


//doctor dashboard

Route::get('/dashboard/rayEmployee', function () {
    return view('dashboard.Dashboard_Ray_Employee.dash2');
})->middleware(['auth:rayEmployee'])->name('dashboard.rayEmployee');


// Route::middleware(['auth:rayEmployee'])->group(function () {
//     Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
//     Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
//     Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
// });

Route::middleware(['auth:rayEmployee'])->group(function () {
    Route::get('RayEmployee_completed_invoices',[InvoiceController::class,'completedInvoices']);
    Route::get('view_rays/{id}',[InvoiceController::class,'viewRays'])->name('viewRays');
    Route::resource('RayEmployeeInvoices', InvoiceController::class);
});



//website



//
Route::get('/loginPage', function () {
    return view('dashboard.user.auth.loginPage');
});
