<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DoctorDashboard\DiagnosisController;
use App\Http\Controllers\DoctorDashboard\InvoiceController;
use App\Http\Controllers\DoctorDashboard\LaboratoriesController;
use App\Http\Controllers\DoctorDashboard\PatientDetailsController;
use App\Http\Controllers\DoctorDashboard\RaysController ;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



require __DIR__ . '/auth.php';



//dashboard
Route::get('/', function () {
    return view('welcome');
});


//doctor dashboard

Route::get('/dashboard/doctor', function () {
    return view('dashboard.doctor.dash2');
})->middleware(['auth:doctor', 'verified'])->name('dashboard.doctor');


// Route::middleware(['auth:doctor'])->group(function () {
//     Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
//     Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
//     Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
// });

Route::middleware(['auth:doctor'])->group(function () {

    Route::get('completed_invoices',[InvoiceController::class,'completedInvoices']);
    Route::get('review_invoices',[InvoiceController::class,'reviewInvoices']);
    Route::resource('Invoices', InvoiceController::class);
    Route::post('add_review',[DiagnosisController::class,'add_review'])->name('add_review');
    Route::resource('Diagnosis', DiagnosisController::class);
    Route::resource('Rays', RaysController::class);
    Route::resource('Laboratories', LaboratoriesController::class);
    Route::get('patient_details/{id}',[PatientDetailsController::class, 'index'])->name('patient_detail');
});


Route::get('/loginPage', function () {
    return view('dashboard.user.auth.loginPage');
});
