<?php

use App\Http\Controllers\Auth\AdminController;
use App\Http\Controllers\Auth\DoctorController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\LaboratoryEmployeeController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\PatientController;
use App\Http\Controllers\Auth\RayEmployeeController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Auth\VerifyEmailController;
use Illuminate\Support\Facades\Route;

Route::middleware('guest')->group(function () {
    Route::get('register', [RegisteredUserController::class, 'create'])
                ->name('register');

    Route::post('register', [RegisteredUserController::class, 'store']);

   // user auth
    Route::get('login', [AuthenticatedSessionController::class, 'create'])
                ->name('login');

    Route::post('login', [AuthenticatedSessionController::class, 'store']);

    Route::get('forgot-password', [PasswordResetLinkController::class, 'create'])
                ->name('password.request');

    Route::post('forgot-password', [PasswordResetLinkController::class, 'store'])
                ->name('password.email');

    Route::get('reset-password/{token}', [NewPasswordController::class, 'create'])
                ->name('password.reset');

    Route::post('reset-password', [NewPasswordController::class, 'store'])
                ->name('password.store');
});


//
   //admin auth
   Route::get('loginForm/admin', [AdminController::class, 'create'])
   ->name('loginForm.admin');

Route::post('login/admin', [AdminController::class, 'store'])->name('login.admin');

Route::post('logout/admin', [AdminController::class, 'destroy'])
->name('logout.admin');

   //

      //doctor auth
      Route::get('loginForm/doctor', [DoctorController::class, 'create'])
      ->name('loginForm.doctor');

   Route::post('login/doctor', [DoctorController::class, 'store'])->name('login.doctor');

   Route::post('logout/doctor', [DoctorController::class, 'destroy'])
   ->name('logout.doctor');

   Route::post('login/rayEmployee', [RayEmployeeController::class, 'store'])->name('login.rayEmployee');

   Route::post('logout/rayEmployee', [RayEmployeeController::class, 'destroy'])
   ->name('logout.rayEmployee');

   Route::post('login/laboratoryEmployee', [LaboratoryEmployeeController::class, 'store'])->name('login.laboratoryEmployee');

   Route::post('logout/laboratoryEmployee', [LaboratoryEmployeeController::class, 'destroy'])
   ->name('logout.laboratoryEmployee');

   Route::post('login/patient', [PatientController::class, 'store'])->name('login.patient');

   Route::post('logout/patient', [PatientController::class, 'destroy'])
   ->name('logout.patient');

      //
Route::middleware('auth')->group(function () {
    Route::get('verify-email', EmailVerificationPromptController::class)
                ->name('verification.notice');

    Route::get('verify-email/{id}/{hash}', VerifyEmailController::class)
                ->middleware(['signed', 'throttle:6,1'])
                ->name('verification.verify');

    Route::post('email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
                ->middleware('throttle:6,1')
                ->name('verification.send');

    Route::get('confirm-password', [ConfirmablePasswordController::class, 'show'])
                ->name('password.confirm');

    Route::post('confirm-password', [ConfirmablePasswordController::class, 'store']);

    Route::put('password', [PasswordController::class, 'update'])->name('password.update');

    Route::post('logout', [AuthenticatedSessionController::class, 'destroy'])
                ->name('logout');
});
