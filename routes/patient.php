<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dashboard_Patient\PatientController;
use App\Http\Controllers\Dashboard_Ray_Employee\InvoiceController;
use App\Http\Controllers\Dashboard_Laboratorie_Employee\InvoiceController as LabInvoices;


require __DIR__ . '/auth.php';



//dashboard
Route::get('/', function () {
    return view('welcome');
});


//patient dashboard

Route::get('/dashboard/patient', function () {
    return view('dashboard.Dashboard_Patient.dash2');
})->middleware(['auth:patient', 'verified'])->name('dashboard.patient');


// Route::middleware(['auth:patient'])->group(function () {
//     Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
//     Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
//     Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
// });

Route::middleware(['auth:patient'])->group(function () {

    Route::get('invoices',[PatientController::class,'Invoices'])->name('invoices.patient');
    Route::get('laboratories',[PatientController::class,'Laboratories'])->name('laboratories.patient');
    Route::get('view_labs_patient/{id}',[LabInvoices::class,'viewLabs'])->name('viewLabs.patient');
    Route::get('rays',[PatientController::class,'Rays'])->name('rays.patient');
    Route::get('view_rays_patient/{id}',[InvoiceController::class,'viewRays'])->name('viewRays.patient');
});


Route::get('/loginPage', function () {
    return view('dashboard.user.auth.loginPage');
});
