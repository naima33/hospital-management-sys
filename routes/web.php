<?php

use App\Http\Controllers\Auth\GoogleLoginController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dashboard\DoctorController;
use App\Http\Controllers\Dashboard\SectionController;
use App\Http\Controllers\Dashboard\SingleServiceController;
use App\Http\Controllers\Dashboard\GroupServiceController;
use App\Http\Controllers\Dashboard\InsuranceController;
use App\Http\Controllers\Dashboard\AmbulanceController;
use App\Http\Controllers\Dashboard\LaboratoryEmployeeController;
use App\Http\Controllers\Dashboard\PatientController;
use App\Http\Controllers\Dashboard\PaymentController;
use App\Http\Controllers\Dashboard\RayEmployeeController;
use App\Http\Controllers\Dashboard\ReceiptController;
use App\Http\Controllers\Dashboard\SingleInvoiceController;
use App\Http\Controllers\Dashboard_Ray_Employee\InvoiceController as RayEmployeeInvoices;
use App\Http\Controllers\Dashboard_Laboratorie_Employee\InvoiceController as LaboratoryEmployeeInvoices;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::middleware('auth')->group(function () {
//     Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
//     Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
//     Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
// });

require __DIR__ . '/auth.php';



//dashboard
Route::get('/', function () {
    return view('welcome');
});

//user dashboard
Route::get('/dashboard/user', function () {
    return view('dashboard.user.dash2');
})->middleware(['auth', 'verified'])->name('dashboard.user');


//admin dashboard

Route::get('/dashboard/admin', function () {
    return view('dashboard.admin.dash2');
})->middleware(['auth:admin'])->name('dashboard.admin');



Route::middleware(['auth:admin,rayEmployee,laboratoryEmployee,patient,doctor'])->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::middleware(['auth:admin'])->group(function () {
    Route::resource('Sections', SectionController::class);
    Route::patch('doctorActive/{id}',[DoctorController::class,'doctorActive'])->name('doctorActive');
    Route::resource('Doctors', DoctorController::class);

    Route::resource('SingleServices', SingleServiceController::class);
    Route::resource('GroupServices', GroupServiceController::class);
    Route::patch('insuranceActive/{id}',[InsuranceController::class,'insuranceActive'])->name('insuranceActive');
    Route::resource('Insurances', InsuranceController::class);
    Route::resource('Ambulances', AmbulanceController::class);
    Route::resource('Patients', PatientController::class);
    Route::resource('SingleInvoices', SingleInvoiceController::class);
    // Route::resource('GroupInvoices', SingleInvoiceController::class);
    Route::get('printReceiptBill/{id}', [ReceiptController::class,'PrintBill'])->name('printReceiptBill');
    Route::resource('Receipts', ReceiptController::class);
    Route::get('printPaymentBill/{id}', [PaymentController::class,'PrintBill'])->name('printPaymentBill');
    Route::resource('Payments', PaymentController::class);
    Route::resource('RayEmployees', RayEmployeeController::class);
    Route::get('AllRayEmployeeInvoices', [RayEmployeeInvoices::class,'AllInvoices']);
    Route::resource('LaboratoryEmployees', LaboratoryEmployeeController::class);
    Route::get('AllLaboratoryEmployeeInvoices',[ LaboratoryEmployeeInvoices::class,'AllInvoices']);
});



//website

Route::get('/google/redirect', [GoogleLoginController::class, 'redirectToGoogle'])->name('google.redirect');
Route::get('/google/callback', [GoogleLoginController::class, 'handleGoogleCallback'])->name('google.callback');

//
Route::get('/loginPage', function () {
    return view('dashboard.user.auth.loginPage');
});
