{{-- edit section --}}



<!-- Modal -->
<div class="modal fade" id="edit{{$section->id}}" tabindex="-1" role="dialog" aria-labelledby="editModalLabel{{ $section->id }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="editModalLabel{{ $section->id }}">update Section</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ route('Sections.update',[$section->id]) }}">
            {{method_field('patch')}}

            {{ csrf_field() }}
            @csrf
        <div class="modal-body">

                <div class="form-row">
                    <div class="col-lg-7 w-75 mx-auto">
                        <input type="hidden" name="id" value="{{$section->id}}">
                        <div class="form-group mb-2">
                            <label for="name">Name:</label>
                            <input type="text" id="name" class="form-control"  placeholder="name"
                            type="text" name="name" value="{{$section->name}}" required autofocus autocomplete="name"
                            >
                          </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="description">Description:</label>
                          <input type="text" id="description" class="form-control"  placeholder="description"
                          type="text" name="description" value="{{$section->description}}" required autofocus autocomplete="description"
                          >
                        </div>
                    </div>
                    {{-- <div class="col-7 w-100 text-center">
                        <button type="submit" class="btn btn-primary mb-2 w-75" >save</button>
                    </div> --}}

                </div>

                </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
      </div>
    </div>
  </div>
