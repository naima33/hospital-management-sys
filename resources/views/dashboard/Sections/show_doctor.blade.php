@extends('layouts.dashboard.master')

@section('title')
  Doctors table
@endsection

@push('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
<link href="https://cdn.datatables.net/2.0.4/css/dataTables.bootstrap4.css" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">

    <style>
        body {
            background-color: #fff;
        }
        .nav li a {
            color: white !important;
        }
    </style>
@endpush

@section('content')
        @include('layouts.dashboard.main-headerbar')

    <main class="main mt-5 pt-3">
        <section>
            <div class="container-fluid">
                <div class="row ">
                    @include('layouts.dashboard.main-sidebar')
                    <div class="col py-3 mt-3">
                        <button type="button" class="btn btn-success mb-3" data-toggle="modal" data-target="#add">
                            Add Doctor
                          </button>

                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>image profile</th>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Price</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    <th>Appointments</th>
                                    <th>Oparations</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($doctors as $doctor)
                                <tr>
                                    <td>{{$doctor->id}}</td>
                                    <td class="text-center align-middle">
                                        @if (!empty($doctor->image->filename))
                                            <a href="" class="rounded-1">
                                                <img class="img-responsive rounded-circle" width="50" height="40"
                                                    src="{{ asset('storage/doctors/' . $doctor->image->filename) }}">

                                            </a>
                                    </td>
                                @else
                                    <a href="">
                                        {{-- <img class="img-responsive" width="100%"  src="{{asset('storage/doctors/'.$doctor->image->filename)}}"> --}}

                                    </a>
                            @endif
                            </td>
                                    <td>{{$doctor->full_name}}</td>
                                    <td>{{$doctor->email}}</td>
                                    <td>{{$doctor->price}}</td>
                                    <td>{{$doctor->phone}}</td>
                                    <td>
                                        <form method="POST" action="{{ route('doctorActive', [$doctor->id]) }}">
                                            {{ method_field('patch') }}

                                            {{ csrf_field() }}
                                            @csrf
                                            <input type='hidden' value='off' name='status'>
                                            <a href="{{ url('doctorActive', [$doctor->id]) }}"
                                                onclick="event.preventDefault();
                                            this.closest('form').submit();">
                                                <div class="form-check form-switch">
                                                    <input class="form-check-input" name="status" type="checkbox"
                                                        role="switch" id="flexSwitchCheckChecked"
                                                        {{ $doctor->status == 1 ? 'checked' : '' }}>

                                                </div>
                                            </a>
                                        </form>

                                    </td>
                                    <td>
                                    @foreach (json_decode($doctor->appointments) as $appointment)
                                        {{ $appointment }}
                                        @if (!$loop->last)
                                            ,
                                        @endif
                                    @endforeach
                                    {{-- {{ $doctor->appointments }} --}}
                                </td>
                                    <td>
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit{{$doctor->id}}"> <i class="bi bi-pencil-fill"></i></button>
                                        <button type="button" class="btn btn-danger"  data-toggle="modal" data-target="#delete{{$doctor->id}}"> <i class="bi bi-trash3-fill"></i></button>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>

                        </table>
                    </div>
                    @include('dashboard.Doctors.add')
                    @foreach ($doctors as $doctor)
                         @include('dashboard.Doctors.edit')
                         @include('dashboard.Doctors.delete')
                    @endforeach

                </div>

            </div>
        </section>

    </main>
@endsection


@push('scripts')
<script defer src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script defer src="https://cdn.datatables.net/2.0.4/js/dataTables.js"></script>
    <script defer src="https://cdn.datatables.net/2.0.4/js/dataTables.bootstrap4.js"></script>
    <script>
$(document).ready(function() {
           new DataTable('#example');
        });
    </script>
@endpush
