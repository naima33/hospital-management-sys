@extends('layouts.dashboard.master')

@section('title')
    Add Single Service Invoices
@endsection

@push('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link href="https://cdn.datatables.net/2.0.4/css/dataTables.bootstrap4.css"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">

    <style>
        body {
            background-color: #fff;
        }

        .nav li a {
            color: white !important;
        }

        #ServicesTable {
            display: none;
        }
    </style>
@endpush

@section('content')
    @include('layouts.dashboard.main-headerbar')

    <main class="main mt-5 pt-3">
        <section>
            <div class="container-fluid ">
                <div class="row ">
                    @include('layouts.dashboard.main-sidebar')
                    <div class="col-10 col-sm-10  col-md-10 col-lg-10 col-xl-10 pb-3 mt-1 ps-0">
                        <div class="container-fluid px-0">
                            <h5 class="text-light py-3 ps-2" style="background-color: rgb(16, 16, 17)"> Add Service Invoices
                            </h5>
                        </div>

                        <div class="container mt-3 py-2 ps-3">
                            <form method="POST" action="{{ route('SingleInvoices.store') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group mb-2">
                                            <label for="name">Patient Name:</label>
                                            <select class="form-select" aria-label="Default select example" id="patient_id"
                                                name="patient_id">
                                                <option selected>Open this select menu</option>
                                                @foreach ($patients as $patient)
                                                    <option value="{{ $patient->id }}">{{ $patient->full_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group mb-2">
                                            <label for="name">Doctor Name:</label>
                                            <select class="form-select doctor" aria-label="Default select example"
                                                id="doctor_id" name="doctor_id">
                                                <option selected>Open this select menu</option>
                                                @foreach ($doctors as $doctor)
                                                    <option value="{{ $doctor->id }}"
                                                        section="{{ $doctor->section->name }}">{{ $doctor->full_name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group mb-2">
                                            <label for="name">section:</label>
                                            <input type="text" id="section" class="form-control section"
                                                placeholder="section" name="section_id" autofocus>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group mb-2">
                                            <label for="name">invoice date:</label>
                                            <input type="date" id="invoice_date" class="form-control"
                                                placeholder="invoice date" name="invoice_date" autofocus>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group mb-2">
                                            <label for="name">Invoice Type:</label>
                                            <select class="form-select" aria-label="Default select example"
                                                id="invoice_type" name="invoice_type">
                                                <option selected>Open this select menu</option>
                                                <option value="single">single</option>
                                                <option value="group">group</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group mb-2">
                                            <label for="name"> Type:</label>
                                            <select class="form-select" aria-label="Default select example" id="type"
                                                name="type">
                                                <option selected>Open this select menu</option>
                                                <option value="1">آجل</option>
                                                <option value="2">نقدي</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class=" row border border-1 mt-1 mx-3 mb-2 py-2">
                                        <div class="col-lg-4">
                                            <div class="form-group mb-2">
                                                <label for="name">Service:</label>
                                                <select class="form-select service" aria-label="Default select example"
                                                    id="service_id" name="service_id">
                                                    <option selected disabled>Open this select menu</option>
                                                    @foreach ($services as $service)
                                                        <option value="{{ $service->id }}" price="{{ $service->price }}"
                                                            service-data="{{$service}}" >
                                                            {{ $service->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group mb-2">
                                                <label for="price">Price:</label>
                                                <input type="text" id="price" class="form-control price"
                                                    placeholder="price" name="price" required autofocus
                                                    autocomplete="price">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group mb-2">
                                                <label for="email">discount value:</label>
                                                <input type="text" id="discount_value" class="form-control discount"
                                                    placeholder="discount value" name="discount_value" required autofocus
                                                    autocomplete="discount_value">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group mb-2">
                                                <label for="tax_rate">tax rate:</label>
                                                <input type="text" id="tax_rate" class="form-control tax-rate"
                                                    placeholder="tax rate" name="tax_rate" required autofocus
                                                    autocomplete="tax_rate">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group mb-2">
                                                <label for="tax_rate">total with tax:</label>
                                                <input type="text" id="tax_value" class="form-control tax-value"
                                                    placeholder="total tax" name="tax_value" required autofocus
                                                    autocomplete="tax_value">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group mb-2">
                                                <label for="tax_rate">Total:</label>
                                                <input type="text" id="total" class="form-control total"
                                                    placeholder="total" name="total" required autofocus
                                                    autocomplete="total">
                                            </div>
                                        </div>

                                    </div>





                                    {{-- <div class="col-7 w-100 text-center">
                                    <button type="submit" class="btn btn-primary mb-2 w-75" >save</button>
                                </div> --}}

                                </div>

                                <button type="submit" class="btn btn-primary">Save</button>
                            </form>
                        </div>
                    </div>
                    {{-- @include('dashboard.Sections.add')
                    @foreach ($sections as $section)
                         @include('dashboard.Sections.edit')
                         @include('dashboard.Sections.delete')
                    @endforeach --}}

                </div>

            </div>
        </section>

    </main>
@endsection


@push('scripts')
    <script>
        $(document).ready(function() {
            $('.doctor').on("change", function() {
                let section = $('option:selected', this).attr('section');
                console.log(section);
                $('.section').val(section);
            });

            $('.service').on("change", function() {
                let price = $('option:selected', this).attr('price');
                const serviceed = $('option:selected', this).attr('service-data');
                console.log(price);
                var name=serviceed.name;
                console.log(name);
                $('.price').val(price);
                $('.total').val(price);





            });





        });
    </script>
@endpush
