<form method="POST" action="{{ route('Receipts.update', [$Receipt->id]) }}">
    {{ method_field('patch') }}

    {{ csrf_field() }}
    @csrf
    <div class="modal-body">

        <div class="form-row">
            <input type="hidden" name="id" value="{{ $Receipt->id }}">
            <div class="col-lg-7 w-75 mx-auto">
                <div class="form-group mb-2">
                    <label for="name">Data:</label>
                    <input type="date" id="date" class="form-control" placeholder="date"
                        name="date" required autofocus autocomplete="date" value="{{ $Receipt->date }}">
                </div>
            </div>
            <div class="col-lg-7 w-75 mx-auto">
                <div class="form-group mb-2">
                    <label for="amount">amount:</label>
                    <input type="text" id="amount" class="form-control" placeholder="amount"
                        name="amount" required autofocus autocomplete="amount"
                        value="{{ $Receipt->amount }}">
                </div>
            </div>
            <div class="col-lg-7 w-75 mx-auto">
                <div class="form-group mb-2">
                    <label for="description">description:</label>
                    <input type="text" id="description" class="form-control" placeholder="description"
                        name="description" autofocus autocomplete="description"
                        value="{{ $Receipt->description }}">
                </div>
            </div>
        </div>


        {{-- <div class="col-7 w-100 text-center">
            <button type="submit" class="btn btn-primary mb-2 w-75" >save</button>
        </div> --}}

    </div>

</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<button type="submit" class="btn btn-primary">Save</button>
</div>
</form>