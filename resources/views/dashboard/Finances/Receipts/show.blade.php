@extends('layouts.dashboard.master')

@section('title')
  Receipts table
@endsection

@push('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
<link href="https://cdn.datatables.net/2.0.4/css/dataTables.bootstrap4.css" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">

    <style>
        body {
            background-color: #fff;
        }
        .nav li a {
            color: white !important;
        }
    </style>
@endpush
@section('content')
        @include('layouts.dashboard.main-headerbar')

    <main class="main mt-5 pt-3">
        <section>
            <div class="container-fluid">
                <div class="row ">
                    {{-- @include('layouts.dashboard.main-sidebar') --}}
                    <div class="col-10 col-sm-10  col-md-10 col-lg-10 col-xl-10 py-3 mt-3">



                            <div style="text-align: center;">فاتورة سند قبض</div>



                            <br><br>
                            <table class="table  table-responsive table-striped table-bordered">
                                <tr>
                                    <th>رقم الفاتورة</th>
                                    <th>السعر</th>
                                    <th>اسم المريض</th>
                                    <th>التاريخ</th>
                                </tr>

                                <tr>
                                    <td>{{ $Receipt['id'] }}</td>
                                    <td>{{ $Receipt['amount'] }}</td>
                                    <td>{{ $Receipt['patient']['full_name'] }}</td>
                                    <td>{{ $Receipt['date'] }}</td>

                                </tr>



                            </table>



                            <button type="submit" class="btn btn-primary"><a href="{{route('printReceiptBill',[$Receipt['id']])}}">Print</a></button>
                            </div>
                    </div>
                </div>

            </div>
        </section>

    </main>
@endsection


