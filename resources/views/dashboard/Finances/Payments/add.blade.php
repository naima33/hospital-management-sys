@extends('layouts.dashboard.master')

@section('title')
    Add Single Service Invoices
@endsection

@push('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link href="https://cdn.datatables.net/2.0.4/css/dataTables.bootstrap4.css"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">

    <style>
        body {
            background-color: #fff;
        }

        .nav li a {
            color: white !important;
        }

        #ServicesTable {
            display: none;
        }
    </style>
@endpush

@section('content')
    @include('layouts.dashboard.main-headerbar')

    <main class="main mt-5 pt-3">
        <section>
            <div class="container-fluid ">
                <div class="row ">
                    @include('layouts.dashboard.main-sidebar')
                    <div class="col-10 col-sm-10  col-md-10 col-lg-10 col-xl-10 pb-3 mt-1 ps-0">
                        <div class="container-fluid px-0">
                            <h5 class="text-light py-3 ps-2" style="background-color: rgb(16, 16, 17)"> Add Receipt Invoice
                            </h5>
                        </div>

                        <div class="container mt-3 py-2 ps-3">
                            <form method="POST" action="{{ route('Payments.store') }}">
                                @csrf
                                    <div class="border border-1 p-3 mb-3 ps-4">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="form-group mb-2">
                                                    <label for="name">Patient Name:</label>
                                                    <select class="form-select" aria-label="Default select example" id="patient_id"
                                                        name="patient_id">
                                                        <option selected>Open this select menu</option>
                                                        @foreach ($patients as $patient)
                                                            <option value="{{ $patient->id }}">{{ $patient->full_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="form-group mb-2">
                                                    <label for="name">Date:</label>
                                                    <input type="date" id="date" class="form-control" placeholder="date"
                                                        name="date" required autofocus autocomplete="date">
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="form-group mb-2">
                                                    <label for="amount">amount:</label>
                                                    <input type="text" id="amount" class="form-control" placeholder="amount"
                                                        name="amount" required autofocus autocomplete="amount">
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="form-group mb-2">
                                                    <label for="description">description:</label>
                                                    <textarea type="text" id="description" class="form-control" placeholder="description"
                                                    name="description" autofocus autocomplete="description" rows="5">

                                                    </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="col-7 w-100 text-center">
                                        <button type="submit" class="btn btn-primary mb-2 w-75" >save</button>
                                    </div> --}}




                                    <button type="submit" class="btn btn-primary">Save</button>

                            </form>
                        </div>
                    </div>
                    {{-- @include('dashboard.Sections.add')
                    @foreach ($sections as $section)
                         @include('dashboard.Sections.edit')
                         @include('dashboard.Sections.delete')
                    @endforeach --}}

                </div>

            </div>
        </section>

    </main>
@endsection


@push('scripts')
    <script>

    </script>
@endpush
