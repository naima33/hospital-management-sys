@extends('layouts.dashboard.laboratory-employee-master')

@section('title')
    Invoices table
@endsection

@push('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link href="https://cdn.datatables.net/2.0.4/css/dataTables.bootstrap4.css"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">

    <style>
        body {
            background-color: #fff;
        }

        .nav li a {
            color: white !important;
        }
    </style>
@endpush


@section('content')
    @include('layouts.dashboard.main-headerbar')

    <main class="main mt-5 pt-3">
        <section>
            <div class="container-fluid">
                <div class="row ">
                    @include('layouts.dashboard.laboratory-employee-sidebar')
                    <div class="col-10 col-sm-10  col-md-10 col-lg-10 col-xl-10 py-3 mt-3">


                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Invoice Date</th>
                                    <th>Patient Name</th>
                                    <th>Doctor Name</th>
                                    <th>Description</th>
                                    <th>Ray Emp Name</th>
                                    <th>Ray Emp Description</th>
                                    <th>Invoice Statu</th>
                                    <th>Operations</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($invoices as $invoice)
                                    <tr>
                                        <td>{{ $invoice->id }}</td>
                                        <td>{{ $invoice->created_at }}</td>

                                        <td>
                                            <a href="{{route('viewLabsImg',[$invoice->id])}}">{{ $invoice->patient->full_name }}</a>
                                        </td>
                                        <td>{{ $invoice->doctor->full_name }}</td>
                                        <td>{{ $invoice->description }}</td>
                                        <td>{{ $invoice->employee->full_name }}</td>
                                        <td>{{ $invoice->employee_description }}</td>


                                        <td class="text-center align-middle">
                                            @if ( $invoice->case==0)
                                            <span class="bg-danger p-1 py-2 rounded-1 ">
                                                In proccing
                                              </span>


                                            @else
                                            <span  class="bg-success p-1 py-2 rounded-1 ">
                                                Completed
                                              </span>
                                            @endif



                                        </td>
                                        <td class="text-center align-middle">
                                            {{-- <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#edit{{ $invoice->id }}"> <i
                                                    class="bi bi-pencil-fill"></i></button>
                                            <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#delete{{ $invoice->id }}"> <i
                                                    class="bi bi-trash3-fill"></i></button> --}}
                                                    <div class="dropdown">
                                                        <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                          Operations
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-end " aria-labelledby="dropdownMenuButton">


                                                            <a href="" class="dropdown-item " data-toggle="modal" data-target="#delete{{ $invoice->id }}"><i class="bi bi-trash text-danger"></i>
                                                                <span class="">delete</span>
                                                            </a>
                                                        </div>
                                                      </div>
                                        </td>
                                    </tr>
                                    {{-- @include('dashboard.Dashboard_Laboratory_Employee.Invoices.add-diagnosis') --}}
                                    {{-- @include('dashboard.doctor.Invoices.add-review')
                                    @include('dashboard.doctor.Invoices.transfer-to-ray')
                                    @include('dashboard.doctor.Invoices.transfer-to-laboratory') --}}
                                @endforeach

                            </tbody>

                        </table>
                    </div>
                    {{-- @if (empty($invoices))

                    @endif --}}
                   {{-- @foreach ($doctors as $doctor)
                         @include('dashboard.Doctors.edit')
                         @include('dashboard.Doctors.delete')
                    @endforeach --}}

                </div>

            </div>
        </section>

    </main>
@endsection


@push('scripts')
    <script defer src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script defer src="https://cdn.datatables.net/2.0.4/js/dataTables.js"></script>
    <script defer src="https://cdn.datatables.net/2.0.4/js/dataTables.bootstrap4.js"></script>
    <script>
        $(document).ready(function() {
            new DataTable('#example');
        });
    </script>
@endpush
