
@extends('layouts.dashboard.master')

@section('title')
    Add Service Group
@endsection

@push('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link href="https://cdn.datatables.net/2.0.4/css/dataTables.bootstrap4.css"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">

    <style>
        body {
         background-color: #fff;
        }

        .nav li a {
            color: white !important;
        }

        #ServicesTable {
            display: none;
        }
    </style>
@endpush

@section('content')
    @include('layouts.dashboard.main-headerbar')

    <main class="main mt-5 pt-3">
        <section>
            <div class="container-fluid ">
                <div class="row ">
                    @include('layouts.dashboard.main-sidebar')
                    <div class="col-10 col-sm-10  col-md-10 col-lg-10 col-xl-10 pb-3 mt-1 ps-0">
                        <div class="container-fluid px-0">
                            <h5 class="text-light py-3 ps-2" style="background-color: rgb(16, 16, 17)"> Add Ambulance</h5>
                        </div>

                       <div class="container mt-3 py-2 ps-3" >
                        <form method="POST" action="{{ route('Ambulances.store') }}">
                            @csrf
                                <div class="row">
                                    <div class="col-lg-3 ">
                                        <div class="form-group mb-2">
                                            <label for="name">car number:</label>
                                            <input type="text" id="name" class="form-control" placeholder="car number"
                                                name="car_number" required autofocus autocomplete="car number">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group mb-2">
                                            <label for="name">car model:</label>
                                            <input id="discount_percentage" class="form-control" placeholder="car model"
                                                type="text" name="car_model" required autofocus>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group mb-2">
                                            <label for="name">car type:</label>
                                            <input id="name" class="form-control" placeholder="car type" type="text"
                                                name="car_type" required autofocus>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group mb-2">
                                            <label for="name">driver name</label>
                                            <input id="name" class="form-control" placeholder="driver name" type="text"
                                                name="driver_name" required autofocus>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group mb-2">
                                            <label for="description">create year:</label>
                                            <input type="text" id="create_year" class="form-control" placeholder="create year"
                                                name="create_year" required autofocus>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group mb-2">
                                            <label for="description">license number:</label>
                                            <input type="text" id="notes" class="form-control" placeholder="license number"
                                                name="license_number" required autofocus>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group mb-2">
                                            <label for="description">driver number:</label>
                                            <input type="text" id="notes" class="form-control" placeholder="driver number"
                                                name="driver_number" required autofocus>
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-lg-11">
                                        <div class="form-group mb-2">
                                            <label for="description">Notes:</label>
                                            <textarea type="text" id="notes" class="form-control" placeholder="notes"
                                            rows="5" name="notes" required autofocus autocomplete="notes">

                                            </textarea>
                                        </div>
                                    </div>
                                <button type="submit" class="btn btn-primary">Save</button>

                        </form>
                       </div>
                    </div>
                    {{-- @include('dashboard.Sections.add')
                    @foreach ($sections as $section)
                         @include('dashboard.Sections.edit')
                         @include('dashboard.Sections.delete')
                    @endforeach --}}

                </div>

            </div>
        </section>

    </main>
@endsection


@push('scripts')
    <script>

</script>
@endpush
