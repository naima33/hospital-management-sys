{{-- edit section --}}
{{-- add section --}}


<!-- Modal -->
<div class="modal fade" id="edit{{$Patient->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">edit Patient</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ route('Patients.update',[$Patient->id]) }}">
            {{method_field('patch')}}

            {{ csrf_field() }}
            @csrf
        <div class="modal-body">

                <div class="form-row">
                    <input type="hidden" name="id" value="{{$Patient->id}}">
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="name">Full name:</label>
                          <input type="text" id="name" class="form-control"  placeholder="full name"
                          type="text" name="full_name"  required autofocus autocomplete="full name"
                          value="{{$Patient->full_name}}"
                          >
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="name">email:</label>
                          <input  id="email" class="form-control"  placeholder="email"
                          type="email" name="email"  required autofocus autocomplete="email"
                          value="{{$Patient->email}}"
                          >
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="name">birth date:</label>
                          <input  id="birth_date" class="form-control"  placeholder="birth date"
                          type="date" name="birth_date"  required autofocus autocomplete="birth date"
                          value="{{$Patient->birth_date}}"
                          >
                        </div>
                    </div>

                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="description">phone:</label>
                          <input id="notes" class="form-control"  placeholder="phone"
                          type="number" name="phone"  required autofocus autocomplete="phone"
                          value="{{$Patient->phone}}"
                          >
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <select class="form-select" aria-label="Default select example" id="gender" name="gender">
                                <option selected>Open this select menu</option>
                                <option value="male">Male</option>
                                <option value="female">Female</option>

                              </select>
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="address">address:</label>
                          <input id="address" class="form-control"  placeholder="address"
                          type="text" name="address"  required autofocus autocomplete="address"
                          value="{{$Patient->address}}"
                          >
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="description">blood type:</label>
                          <input id="blood_type" class="form-control"  placeholder="blood type"
                          type="text" name="blood_type"  required autofocus autocomplete="blood_type"
                          value="{{$Patient->blood_type}}"
                          >
                        </div>
                    </div>
                    {{-- <div class="col-7 w-100 text-center">
                        <button type="submit" class="btn btn-primary mb-2 w-75" >save</button>
                    </div> --}}

                </div>

                </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
      </div>
    </div>
  </div>
