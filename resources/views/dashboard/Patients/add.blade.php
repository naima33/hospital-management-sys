
@extends('layouts.dashboard.master')

@section('title')
    Add Patient
@endsection

@push('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link href="https://cdn.datatables.net/2.0.4/css/dataTables.bootstrap4.css"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">

    <style>
        body {
         background-color: #fff;
        }

        .nav li a {
            color: white !important;
        }

        #ServicesTable {
            display: none;
        }
    </style>
@endpush

@section('content')
    @include('layouts.dashboard.main-headerbar')

    <main class="main mt-5 pt-3">
        <section>
            <div class="container-fluid ">
                <div class="row ">
                    @include('layouts.dashboard.main-sidebar')
                    <div class="col-10 col-sm-10  col-md-10 col-lg-10 col-xl-10 pb-3 mt-1 ps-0">
                        <div class="container-fluid px-0">
                            <h5 class="text-light py-3 ps-2" style="background-color: rgb(16, 16, 17)"> Add Patient</h5>
                        </div>

                       <div class="container mt-3 py-2 ps-3" >
                        <form method="POST" action="{{ route('Patients.store') }}">
                            @csrf
                            <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group mb-2">
                                            <label for="name">Full name:</label>
                                            <input type="text" id="name" class="form-control" placeholder="full name"
                                                type="text" name="full_name" required autofocus autocomplete="full name">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group mb-2">
                                            <label for="name">email:</label>
                                            <input id="email" class="form-control" placeholder="email" type="email"
                                                name="email" required autofocus autocomplete="email">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group mb-2">
                                            <label for="name">birth date:</label>
                                            <input id="birth_date" class="form-control" placeholder="birth date" type="date"
                                                name="birth_date" required autofocus autocomplete="birth date">
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group mb-2">
                                            <label for="description">phone:</label>
                                            <input id="notes" class="form-control" placeholder="phone" type="number"
                                                name="phone" required autofocus autocomplete="phone">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group mb-2">
                                            <label for="description">Gender:</label>
                                            <select class="form-select" aria-label="Default select example" id="gender"
                                                name="gender">
                                                <option selected>Open this select menu</option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group mb-2">
                                            <label for="description">blood type:</label>
                                            <input id="blood_type" class="form-control" placeholder="blood type" type="text"
                                                name="blood_type" required autofocus autocomplete="blood type">
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group mb-2">
                                            <label for="description">address:</label>
                                            <textarea id="address" class="form-control" placeholder="address" type="text"
                                            name="address" required autofocus autocomplete="address">
                                            </textarea>
                                        </div>
                                    </div>

                                    {{-- <div class="col-7 w-100 text-center">
                                    <button type="submit" class="btn btn-primary mb-2 w-75" >save</button>
                                </div> --}}

                                </div>
                                <button type="submit" class="btn btn-primary">Save</button>

                        </form>
                       </div>
                    </div>
                    {{-- @include('dashboard.Sections.add')
                    @foreach ($sections as $section)
                         @include('dashboard.Sections.edit')
                         @include('dashboard.Sections.delete')
                    @endforeach --}}

                </div>

            </div>
        </section>

    </main>
@endsection


@push('scripts')
    <script>

</script>
@endpush
