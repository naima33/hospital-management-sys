@extends('layouts.dashboard.master')

@section('title')
    Patients table
@endsection

@push('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link href="https://cdn.datatables.net/2.0.4/css/dataTables.bootstrap4.css"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">

    <style>
        body {
            background-color: #fff;
        }

        .nav li a {
            color: white !important;
        }
    </style>
@endpush

@section('content')
    @include('layouts.dashboard.main-headerbar')

    <main class="main mt-5 pt-3">
        <section>
            <div class="container-fluid">
                <div class="row ">
                    @include('layouts.dashboard.main-sidebar')
                    <div class="col-10 col-sm-10  col-md-10 col-lg-10 col-xl-10 py-3 mt-3">
                        <div>
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="PatientInfo-tab" data-bs-toggle="tab"
                                        data-bs-target="#PatientInfo" type="button" role="tab"
                                        aria-controls="PatientInfo" aria-selected="true">Patient Info</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="Invoices-tab" data-bs-toggle="tab"
                                        data-bs-target="#Invoices" type="button" role="tab" aria-controls="Invoices"
                                        aria-selected="false">Invoices</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="Payments-tab" data-bs-toggle="tab"
                                        data-bs-target="#Payments" type="button" role="tab" aria-controls="Payments"
                                        aria-selected="false">Payments</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link " id="AccountStatement-tab" data-bs-toggle="tab"
                                        data-bs-target="#AccountStatement" type="button" role="tab"
                                        aria-controls="AccountStatement" aria-selected="true">Account Statement</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="X_Rays-tab" data-bs-toggle="tab" data-bs-target="#X_Rays"
                                        type="button" role="tab" aria-controls="X_Rays"
                                        aria-selected="false">X-rays</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="Laboratory-tab" data-bs-toggle="tab"
                                        data-bs-target="#Laboratory" type="button" role="tab"
                                        aria-controls="Laboratory" aria-selected="false">Laboratory</button>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="PatientInfo" role="tabpanel"
                                    aria-labelledby="PatientInfo-tab">
                                    <div class="border border-2 m-3">
                                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>email</th>
                                                    <th>birth date</th>
                                                    <th>gender</th>
                                                    <th>phone</th>
                                                    <th>address</th>
                                                    <th>blood type</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {{-- @foreach ($Patient as $Patient) --}}
                                                <tr>
                                                    <td>{{ $patient->id }}</td>
                                                    <td>{{ $patient->full_name }}</td>
                                                    <td>{{ $patient->email }}</td>
                                                    <td>{{ $patient->birth_date }}</td>
                                                    <td>{{ $patient->gender }}</td>
                                                    <td>{{ $patient->phone }}</td>
                                                    <td>{{ $patient->address }}</td>
                                                    <td>{{ $patient->blood_group }}</td>
                                                </tr>
                                                {{-- @endforeach --}}

                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="Invoices" role="tabpanel" aria-labelledby="Invoices-tab">
                                    <div class="border border-2 m-3">
                                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Service Name</th>
                                                    <th>Invoice Date</th>
                                                    <th>Total</th>
                                                    <th>Invoice Type</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($invoices as $invoice)
                                                <tr>
                                                    <td>{{ $invoice->id }}</td>
                                                    <td>{{ $invoice->service->name ?? $invoice->group->name  }}</td>
                                                    <td>{{ $invoice->invoice_date }}</td>
                                                    <td>{{ $invoice->total_with_rate }}</td>
                                                    <td>{{ $invoice->invoice_type }}</td>
                                                </tr>
                                                @endforeach
                                                <tr>
                                                </tr>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="Payments" role="tabpanel" aria-labelledby="Payments-tab">
                                    <div class="border border-2 m-3">
                                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Date</th>
                                                    <th>Amount</th>
                                                    <th>Description</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($receipts as $receipt)
                                                <tr>
                                                    <td>{{ $receipt->id }}</td>
                                                    <td>{{ $receipt->date }}</td>
                                                    <td>{{ $receipt->amount }}</td>
                                                    <td>{{ $receipt->description }}</td>

                                                </tr>
                                                @endforeach
                                                <tr>
                                                </tr>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="AccountStatement" role="tabpanel" aria-labelledby="AccountStatement-tab">
                                    <div class="border border-2 m-3">
                                        <table id="example" class="table  table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Date</th>
                                                    <th>Description</th>
                                                    <th>debtor</th>
                                                    <th>Creditor دائن</th>
                                                    <th>Total</th>


                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($PatientAccounts as $PatientAccount)
                                                <tr>
                                                    <td>{{ $PatientAccount->id }}</td>
                                                    <td>{{ $PatientAccount->date }}</td>
                                                    <td>{{ $PatientAccount->description }}</td>
                                                    <td>{{ $PatientAccount->debit }}</td>
                                                    <td>{{ $PatientAccount->credit }}</td>

                                                </tr>
                                                @endforeach
                                                <tr>
                                                    <th colspan="3"  class="p-0"><div class="alert alert-info w-100 mb-0 rounded-0">The final balance</div></th>
                                                    <td class="alert alert-primary p-0">
                                                        <div class="w-100 mb-0 rounded-0 alert alert-primary">
                                                            {{ number_format($debit=$PatientAccounts->sum('debit'))}}
                                                        </div>
                                                        </td>
                                                    <td class="alert alert-primary p-0">
                                                        <div class="alert alert-primary w-100 mb-0 rounded-0">
                                                            {{ number_format($credit=$PatientAccounts->sum('credit'))}}
                                                        </div>
                                                        </td>
                                                    <td class="alert alert-danger p-0">
                                                        <div class="alert alert-danger text-danger  w-100 mb-0 rounded-0">
                                                               {{$debit-$credit}}
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="X_Rays" role="tabpanel" aria-labelledby="X_Rays-tab">
                                    <div class="border border-2 m-3">
                                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>request Description</th>
                                                    <th>Patient Name</th>
                                                    <th>Doctor Name</th>
                                                    <th>Invoice Date</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($ray as $patient_ray)
                                                    <tr>
                                                        <td>{{ $patient_ray->id }}</td>
                                                        <td>{{ $patient_ray->description }}</td>
                                                        <td><a
                                                                href="{{ route('Invoices.show', [$patient_ray->id]) }}">{{ $patient_ray->patient->full_name }}</a>
                                                        </td>
                                                        <td>{{ $patient_ray->doctor->full_name }}</td>
                                                        <td>{{ $patient_ray->created_at }}</td>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                </tr>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="Laboratory" role="tabpanel"
                                    aria-labelledby="Laboratory-tab">
                                    <div class="border border-2 m-3">
                                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>request Description</th>
                                                    <th>Patient Name</th>
                                                    <th>Doctor Name</th>
                                                    <th>Invoice Date</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($laboratory as $patient_lab)
                                                    <tr>
                                                        <td>{{ $patient_lab->id }}</td>
                                                        <td>{{ $patient_lab->description }}</td>
                                                        <td><a
                                                                href="{{ route('Invoices.show', [$patient_lab->id]) }}">{{ $patient_lab->patient->full_name }}</a>
                                                        </td>
                                                        <td>{{ $patient_lab->doctor->full_name }}</td>
                                                        <td>{{ $patient_lab->created_at }}</td>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                </tr>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- @include('dashboard.Patients.add') --}}
                    {{-- @foreach ($Patients as $Patient) --}}
                    {{-- @include('dashboard.Patients.edit') --}}
                    {{-- @include('dashboard.Patients.delete') --}}
                    {{-- @endforeach --}}

                </div>

            </div>
        </section>

    </main>
@endsection


@push('scripts')
    <script defer src="https://code.jquery.com/jquery-3.7.1.js"></script>
    {{-- <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script defer src="https://cdn.datatables.net/2.0.4/js/dataTables.js"></script>
    <script defer src="https://cdn.datatables.net/2.0.4/js/dataTables.bootstrap4.js"></script>
    <script>
$(document).ready(function() {
           new DataTable('#example');
        }); --}}
    </script>
@endpush
