{{-- edit section --}}
{{-- add section --}}


<!-- Modal -->
<div class="modal fade" id="edit{{$insurance->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">edit insurance</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ route('Insurances.update',[$insurance->id]) }}">
            {{method_field('patch')}}

            {{ csrf_field() }}
            @csrf
        <div class="modal-body">

                <div class="form-row">
                    <input type="hidden" name="id" value="{{$insurance->id}}">
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="name">insurance name:</label>
                          <input type="text" id="name" class="form-control"  placeholder="insurance name"
                          type="text" name="insurance_name"  required autofocus autocomplete="insurance name"
                          value="{{$insurance->name}}"
                          >
                        </div>
                    </div>

                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="name">discount percentage:</label>
                          <input type="text" id="discount_percentage" class="form-control"  placeholder="discount percentage"
                          type="text" name="discount_percentage"  required autofocus autocomplete="discount_percentage"
                          value="{{$insurance->discount_percentage}}"
                          >
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="name">company rate:</label>
                          <input type="text" id="name" class="form-control"  placeholder="company rate"
                          type="text" name="company_rate"  required autofocus autocomplete="company_rate"
                          value="{{$insurance->company_rate}}"
                          >
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <input type='hidden' value='off' name='status'>
                            <input class="form-check-input" type="checkbox"  id="status" name="status"
                            {{$insurance->status=='1' ? " value=on checked" :""}}>
                            <label for="status">status</label>
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="description">Notes:</label>
                          <input type="text" id="notes" class="form-control"  placeholder="notes"
                          type="text" name="notes"  autofocus autocomplete="notes"
                          value="{{$insurance->notes}}"
                          >
                        </div>
                    </div>
                    {{-- <div class="col-7 w-100 text-center">
                        <button type="submit" class="btn btn-primary mb-2 w-75" >save</button>
                    </div> --}}

                </div>

                </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
      </div>
    </div>
  </div>
