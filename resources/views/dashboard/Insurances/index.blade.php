@extends('layouts.dashboard.master')

@section('title')
  Doctors table
@endsection

@push('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
<link href="https://cdn.datatables.net/2.0.4/css/dataTables.bootstrap4.css" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">

    <style>
        body {
            background-color: #fff;
        }
        .nav li a {
            color: white !important;
        }
    </style>
@endpush

@section('content')
        @include('layouts.dashboard.main-headerbar')

    <main class="main mt-5 pt-3">
        <section>
            <div class="container-fluid">
                <div class="row ">
                    @include('layouts.dashboard.main-sidebar')
                    <div class="col-10 col-sm-10  col-md-10 col-lg-10 col-xl-10 py-3 mt-3">
                        <button type="button" class="btn btn-success mb-3" >
                            <a href="{{route('Insurances.create')}}">
                                Add Insurance
                            </a>

                          </button>

                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Discount Percentage</th>
                                    <th>Company Rate</th>
                                    <th>Notes</th>
                                    <th>Status</th>
                                    <th>Operations</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($insurances as $insurance)
                                <tr>
                                    <td>{{$insurance->id}}</td>
                                    <td>{{$insurance->name}}</td>
                                    <td>{{$insurance->discount_percentage}}</td>
                                    <td>{{$insurance->company_rate}}</td>
                                    <td>{{$insurance->notes}}</td>
                                    <td>
                                        <form method="POST" action="{{route('insuranceActive',[$insurance->id])}}">
                                            {{ method_field('patch') }}

                                            {{ csrf_field() }}
                                            @csrf
                                            <input type='hidden' value='off' name='status'>
                                            <a href="{{route('insuranceActive',[$insurance->id])}}"
                                                onclick="event.preventDefault();
                                                    this.closest('form').submit();">
                                                <div class="form-check form-switch">
                                                    <input class="form-check-input" name="status" type="checkbox"
                                                        role="switch" id="flexSwitchCheckChecked"
                                                        {{ $insurance->status == 1 ? 'checked' : '' }}>

                                                </div>
                                            </a>
                                        </form>
                                    </td>

                                    <td>
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit{{$insurance->id}}"> <i class="bi bi-pencil-fill"></i></button>
                                        <button type="button" class="btn btn-danger"  data-toggle="modal" data-target="#delete{{$insurance->id}}"> <i class="bi bi-trash3-fill"></i></button>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>

                        </table>
                    </div>
                    {{-- @include('dashboard.Insurances.add') --}}
                    @foreach ($insurances as $insurance)
                         @include('dashboard.Insurances.edit')
                         @include('dashboard.Insurances.delete')
                    @endforeach

                </div>

            </div>
        </section>

    </main>
@endsection


@push('scripts')
<script defer src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script defer src="https://cdn.datatables.net/2.0.4/js/dataTables.js"></script>
    <script defer src="https://cdn.datatables.net/2.0.4/js/dataTables.bootstrap4.js"></script>
    <script>
$(document).ready(function() {
           new DataTable('#example');
        });
    </script>
@endpush
