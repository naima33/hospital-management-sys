
@extends('layouts.dashboard.master')

@section('title')
    Add Service Group
@endsection

@push('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link href="https://cdn.datatables.net/2.0.4/css/dataTables.bootstrap4.css"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">

    <style>
        body {
            background-color: #fff;
        }

        .nav li a {
            color: white !important;
        }

        #ServicesTable {
            display: none;
        }
    </style>
@endpush

@section('content')
    @include('layouts.dashboard.main-headerbar')

    <main class="main mt-5 pt-3">
        <section>
            <div class="container-fluid ">
                <div class="row ">
                    @include('layouts.dashboard.main-sidebar')
                    <div class="col-10 col-sm-10  col-md-10 col-lg-10 col-xl-10 py-3 mt-3">
                        <h5 class="mt-1"> Add Insurance</h5>
                       <div class="container mt-3 py-2" style="background-color: #e0e8eb">
                        <form method="POST" action="{{ route('Insurances.store') }}">
                            @csrf

                                <div class="row">

                                        <div class="form-group mb-2 col-4">
                                            <label for="name">insurance name:</label>
                                            <input type="text" id="name" class="form-control" placeholder="insurance name"
                                                type="text" name="insurance_name" required autofocus
                                                autocomplete="insurance name">
                                        </div>


                                        <div class="form-group col-4 mb-2">
                                            <label for="name">discount percentage:</label>
                                            <input id="discount_percentage" class="form-control" placeholder="discount percentage"
                                                type="number" name="discount_percentage" required autofocus
                                                autocomplete="discount_percentage">
                                        </div>

                                </div>
                                <div class="form-row">
                                    <div class="col-lg-8">
                                        <div class="form-group mb-2">
                                            <label for="name">company rate:</label>
                                            <input id="name" class="form-control" placeholder="company rate" type="number"
                                                name="company_rate" required autofocus autocomplete="company_rate">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">

                                    <div class="col-lg-8">
                                        <div class="form-group mb-2">
                                            <label for="description">Notes:</label>
                                            <textarea type="text" id="notes" class="form-control" placeholder="notes"
                                             name="notes"  autofocus autocomplete="notes" rows="5">

                                            </textarea>

                                        </div>
                                    </div>
                                </div>
                                    {{-- <div class="col-7 w-100 text-center">
                                    <button type="submit" class="btn btn-primary mb-2 w-75" >save</button>
                                </div> --}}



                                <button type="submit" class="btn btn-primary">Save</button>

                        </form>
                       </div>
                    </div>
                    {{-- @include('dashboard.Sections.add')
                    @foreach ($sections as $section)
                         @include('dashboard.Sections.edit')
                         @include('dashboard.Sections.delete')
                    @endforeach --}}

                </div>

            </div>
        </section>

    </main>
@endsection


@push('scripts')
    <script>

</script>
@endpush
