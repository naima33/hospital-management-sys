@extends('layouts.dashboard.master')

@section('title')
    Laboratory Employee table
@endsection


@push('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link href="https://cdn.datatables.net/2.0.4/css/dataTables.bootstrap4.css"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    <style>
        body {
            background-color: #fff;
        }

        .nav li a {
            color: white !important;
        }
    </style>
@endpush


@section('content')
    @include('layouts.dashboard.main-headerbar')

    <main class="main mt-5 pt-3">
        <section>
            <div class="container-fluid">
                <div class="row ">
                    @include('layouts.dashboard.main-sidebar')
                    <div class="col-10 col-sm-10  col-md-10 col-lg-10 col-xl-10 py-3 mt-3">
                        <button type="button" class="btn btn-success mb-3" data-toggle="modal" data-target="#add">
                            Add Laboratory Employee
                        </button>

                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Added At</th>
                                    <th>Operations</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($LaboratoryEmployees as $LaboratoryEmployee)
                                    <tr>
                                        <td>{{ $LaboratoryEmployee->id }}</td>
                                        <td>{{ $LaboratoryEmployee->full_name }}</td>
                                        <td>{{ $LaboratoryEmployee->email }}</td>
                                        <td>{{ $LaboratoryEmployee->created_at->diffForHumans() }}</td>

                                        <td>
                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#edit{{ $LaboratoryEmployee->id }}"> <i
                                                    class="bi bi-pencil-fill"></i></button>
                                            <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#delete{{ $LaboratoryEmployee->id }}"> <i
                                                    class="bi bi-trash3-fill"></i></button>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>

                        </table>
                    </div>
                    @include('dashboard.LaboratoryEmployee.add')
                    @foreach ($LaboratoryEmployees as $LaboratoryEmployee)
                        @include('dashboard.LaboratoryEmployee.edit')
                        @include('dashboard.LaboratoryEmployee.delete')
                    @endforeach

                </div>

            </div>
        </section>

    </main>
@endsection


@push('scripts')
    <script defer src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script defer src="https://cdn.datatables.net/2.0.4/js/dataTables.js"></script>
    <script defer src="https://cdn.datatables.net/2.0.4/js/dataTables.bootstrap4.js"></script>
    <script>
        $(document).ready(function() {
            new DataTable('#example');
        });
    </script>
@endpush
