

<!-- Modal -->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Service</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ route('SingleServices.store') }}">
            @csrf
        <div class="modal-body">

                <div class="form-row">
                    <div class="col-lg-7 w-100 mx-auto">
                        <div class="form-group mb-2">
                            <label for="name">Name:</label>
                          <input type="text" id="name" class="form-control"  placeholder="name"
                          type="text" name="name"  required autofocus autocomplete="name"
                          >
                        </div>
                    </div>
                    <div class="col-lg-7 w-100 mx-auto">
                        <div class="form-group mb-2">
                            <label for="name">Price:</label>
                          <input type="text" id="price" class="form-control"  placeholder="price"
                           name="price"  required autofocus autocomplete="name"
                          >
                        </div>
                    </div>
                    {{-- <div class="col-lg-7 w-100 mx-auto">
                        <div class="form-group mb-2">
                            <input class="form-check-input" type="checkbox"  id="status" name="status">
                            <label for="status"> status</label>
                        </div>
                    </div> --}}
                    <div class="col-lg-7 w-100 mx-auto">
                        <div class="form-group mb-2">
                            <label for="description">Description:</label>
                          <input type="text" id="description" class="form-control"  placeholder="description"
                          type="text" name="description"  autofocus autocomplete="description"
                          >
                        </div>
                    </div>
                    {{-- <div class="col-7 w-100 text-center">
                        <button type="submit" class="btn btn-primary mb-2 w-75" >save</button>
                    </div> --}}

                </div>

                </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
      </div>
    </div>
  </div>
