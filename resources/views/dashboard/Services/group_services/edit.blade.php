@extends('layouts.dashboard.master')

@section('title')
    Add Service Group
@endsection

@push('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link href="https://cdn.datatables.net/2.0.4/css/dataTables.bootstrap4.css"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">

    <style>
        body {
            background-color: #fff;
        }

        .nav li a {
            color: white !important;
        }

        #ServicesTable {
            display: none;
        }
    </style>
@endpush

@section('content')
    @include('layouts.dashboard.main-headerbar')

    <main class="main mt-5 pt-3">
        <section>
            <div class="container-fluid ">
                <div class="row ">
                    @include('layouts.dashboard.main-sidebar')
                    <div class="col py-3 mt-3">
                        <form method="POST" action="{{ route('GroupServices.store') }}">
                            @csrf
                            <div class="form-group my-2">
                                <label for="group_name">group name</label>
                                <input type="text" name="name" class="form-control" id="group_name">
                            </div>

                            <div class="form-group my-2">
                                <label for="exampleFormControlTextarea1">Notes</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>

                            <div class="border-1 px-3">
                                <div class="mt-3">
                                    <button type="button" class="btn btn-outline-primary add-service" id="test"
                                        iteration='1'>Add
                                        Service Group</button>
                                </div>
                                <div class="table-responsive" id="ServicesTable">
                                    <table class="table table-bordered bg-primary my-3 " id="table1">
                                        <thead>
                                            <thead>
                                                <tr>
                                                    <th scope="col-1">#</th>
                                                    <th scope="col-4">Service Name</th>
                                                    <th scope="col-1">Quantity</th>

                                                </tr>
                                            </thead>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>
                                                    <select class="form-select select-box"
                                                        aria-label="Default select example" id="" name="service[]">
                                                        <option selected disabled>Open this select menu</option>
                                                        @foreach ($Services as $Service)
                                                            <option value="{{ $Service->id }}"
                                                                price="{{ $Service->price }}">
                                                                {{ $Service->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td class="">
                                                    <input type="number" class="quantity form-control border border-2" name="quantity[]"
                                                        id="quantity">
                                                </td>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="form-group row my-3">
                                    <label class="col-1 p-0 ms-3 text-danger" for="Total">Total</label>
                                    <div class="col-6">
                                        <input type="text" name="total_before_discount" class="form-control Total"
                                            id="Total" placeholder="Total">
                                    </div>
                                </div>

                                <div class="form-group row my-2">
                                    <label class="col-1 p-0 ms-3 text-danger" for="Discount">Discount</label>
                                    <div class="col-6">
                                        <input type="text" name="discount_value" class="form-control discount"
                                            id="discount" placeholder="discount" value="0">
                                    </div>
                                </div>

                                <div class="form-group row my-2">
                                    <label class="col-1 p-0 ms-3 text-danger" for="tax">tax value</label>
                                    <div class="col-6">
                                        <input type="text" name="tax_rate" class="form-control tax" id="tax"
                                            placeholder="tax" value="0.0">
                                    </div>
                                </div>

                                <div class="form-group row my-2">
                                    <label class="col-1 p-0 ms-3 text-danger" for="total_with_tax">total with tax</label>
                                    <div class="col-6">
                                        <input type="text" name="total_with_tax" class="form-control tax-Total"
                                            id="tax-Total" placeholder="total with tax">
                                    </div>
                                </div>
                            </div>

                            <div class="col-3 w-100 my-3 ms-2">
                                <button type="submit" class="btn btn-outline-success mb-2 ">Add</button>
                            </div>
                        </form>
                    </div>
                    {{-- @include('dashboard.Sections.add')
                    @foreach ($sections as $section)
                         @include('dashboard.Sections.edit')
                         @include('dashboard.Sections.delete')
                    @endforeach --}}

                </div>

            </div>
        </section>

    </main>
@endsection


@push('scripts')
    <script>
        $(document).ready(function() {
            let i=0;
            // console.log(CountRows());
            $('.add-service').click(function() {
                let id = $(this).attr('id');
                let iteration = $(this).attr('iteration');
                console.log(iteration);
                $(this).attr('iteration', parseInt(iteration) + 1);
                if (parseInt(iteration) > 1) {
                    i=iteration;
                    var rowNumber = iteration;
                    var newRow = $("<tr></tr>");

                    var thCell = $("<th scope='row'></th>").text(rowNumber);
                    var tdSelectCell = $("<td></td>");
                    var selectBox = $("<select></select>").addClass("form-select select-box")
                        .attr("aria-label", "Default select example")
                        .attr("id", "")
                        .attr("name", "service[]");
                    var defaultOption = $("<option></option>").attr("selected", "selected").attr("disabled",
                            "disabled")
                        .text("Open this select menu");

                    selectBox.append(defaultOption);

                    // Iterate through Services array
                    @foreach ($Services as $Service)
                        var option = $("<option></option>").attr("value", "{{ $Service->id }}")
                            .attr("price", "{{ $Service->price }}")
                            .text("{{ $Service->name }}");
                        selectBox.append(option);
                    @endforeach

                    tdSelectCell.append(selectBox);

                    var tdQuantityCell = $("<td></td>").addClass("");
                    var quantityInput = $("<input>").attr("type", "number")
                        .addClass("quantity form-control border border-2")
                        .attr("name", "quantity[]")
                        // .attr("value", "5")
                        .attr("id", "quantity");

                    tdQuantityCell.append(quantityInput);

                    // var tdButtonCell = $("<td></td>").text("add");

                    newRow.append(thCell);
                    newRow.append(tdSelectCell);
                    newRow.append(tdQuantityCell);
                    // newRow.append(tdButtonCell);

                    $("#table1").append(newRow);

                    rowNumber++; // Increment row number
                    console.log('add row');
                    //     $('#table1 tr').each(function() {
                    //     var rowContent = $(this).text();
                    //     console.log(rowContent);
                    // });
                }

                $('#ServicesTable').show();
                // $('.select-box').each(function(){
                //     id===$(this).attr('id')? $(this).show() :  $(this).hide();
                // });

            });

            $('#table1').on("change",".select-box",  function() {
                // console.log('ddd');
                let quantity=$('.quantity');
                console.log(quantity);
               let price = $('option:selected', this).attr('price');
                console.log(price);
                if(parseInt(i) > 1)
                {
                    t=$('.Total').val();
                    price=parseInt(price)+parseInt(t);
                    console.log(t);
                    $('.Total').val(price);
                }
                else
                $('.Total').val(price);

                $(".tax-Total").val(price);
                $('.discount').on("change", function() {

                    discount = $('.discount').val();
                    console.log(discount);
                    sub_total = price - discount;
                    // (((int)$data['tax_rate']/100)*$subTotal)+$subTotal,
                    total_with_tax = (parseInt(sub_total));
                    // console.log(total_with_tax);
                    $(".tax-Total").val(total_with_tax);



                });

                $('.tax').on("change", function() {

                    tax = $('.tax').val();
                    console.log(tax);
                    sub_total = price - discount;
                    // (((int)$data['tax_rate']/100)*$subTotal)+$subTotal,
                    total_with_tax = ((parseInt(tax) / 100) * parseInt(sub_total)) + parseInt(
                        sub_total);
                    // console.log(total_with_tax);
                    $(".tax-Total").val(total_with_tax);


                });
                // tax=$('.tax').val();
                // console.log(tax);


            });

        });
    </script>
@endpush
