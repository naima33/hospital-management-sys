@extends('layouts.dashboard.master-doctor')

@section('title')
    show Rays Images
@endsection

@push('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link href="https://cdn.datatables.net/2.0.4/css/dataTables.bootstrap4.css"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">

    <style>
        body {
            background-color: #fff;
        }

        .nav li a {
            color: white !important;
        }
    </style>
@endpush


@section('content')
    @include('layouts.dashboard.main-headerbar')

    <main class="main mt-5 pt-3">
        <section>
            <div class="container-fluid">
                <div class="row ">
                    @include('layouts.dashboard.doctor-sidebar')
                    <div class="col-10 col-sm-10  col-md-10 col-lg-10 col-xl-10 py-3 mt-3">
                        <div class="demo-gallery">

                            {{-- <ul class="list-unstyled row row-sm pr-0">
                              @foreach ($rays->images as $image)
                              <li class="col-sm-6 col-lg-4" data-responsive="{{asset('storage/rays/'.$image->filename)}}">
                                 <a href="">
                                     <img class="img-responsive" width="100%" height="300px" src="{{asset('storage/rays/'.$image->filename)}}">

                                 </a>
                              </li>
                              @endforeach

                            </ul> --}}

                            <div class="container">

                              <h2 class="fw-light text-center text-lg-start mt-4 mb-0">Ray for patient {{$rays->patient->full_name}}</h2>

                              <hr class="mt-3 mb-3">
                              <h5 class="fw-light text-center text-lg-start mt-4 mb-4">Diagnose: {{$rays->employee_description}} </h5>


                              <div class="row text-center text-lg-start">

                                  @foreach ($rays->images as $image)
                                  {{-- <li class="col-sm-6 col-lg-4" data-responsive="{{asset('storage/rays/'.$image->filename)}}">
                                     <a href="">
                                         <img class="img-responsive" width="100%" height="300px" src="{{asset('storage/rays/'.$image->filename)}}">

                                     </a>
                                  </li> --}}
                                  <div class="col-lg-3 col-md-4 col-6" >
                                    <a href="#" class="d-block mb-4 h-100">
                                      <img class="img-fluid img-thumbnail" src="{{asset('storage/rays/'.$image->filename)}}">
                                    </a>
                                  </div>
                                  @endforeach










                              </div>

                            </div>
                          </div>
{{-- content --}}

                    </div>
                    {{-- @if (empty($invoices))

                    @endif --}}
                   {{-- @foreach ($doctors as $doctor)
                         @include('dashboard.Doctors.edit')
                         @include('dashboard.Doctors.delete')
                    @endforeach --}}

                </div>

            </div>
        </section>

    </main>
@endsection


@push('scripts')
    <script defer src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script defer src="https://cdn.datatables.net/2.0.4/js/dataTables.js"></script>
    <script defer src="https://cdn.datatables.net/2.0.4/js/dataTables.bootstrap4.js"></script>
    <script>
        $(document).ready(function() {
            new DataTable('#example');
        });
    </script>
@endpush
