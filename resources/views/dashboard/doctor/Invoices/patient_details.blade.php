@extends('layouts.dashboard.master-doctor')

@section('title')
    show Rays Images
@endsection

@push('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link href="https://cdn.datatables.net/2.0.4/css/dataTables.bootstrap4.css"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">

    <style>
        body {
            background-color: #fff;
        }

        .nav li a {
            color: white !important;
        }

        /* The actual timeline (the vertical ruler) */
        .main-timeline {
            position: relative;
        }

        /* The actual timeline (the vertical ruler) */
        .main-timeline::after {
            content: "";
            position: absolute;
            width: 6px;
            background-color: #939597;
            top: 0;
            bottom: 0;
            left: 50%;
            margin-left: -3px;
        }

        /* Container around content */
        .timeline {
            position: relative;
            background-color: inherit;
            width: 50%;
        }

        /* The circles on the timeline */
        .timeline::after {
            content: "";
            position: absolute;
            width: 25px;
            height: 25px;
            right: -13px;
            background-color: #939597;
            border: 5px solid #f5df4d;
            top: 15px;
            border-radius: 50%;
            z-index: 1;
        }

        /* Place the container to the left */
        .left {
            padding: 0px 40px 20px 0px;
            left: 0;
        }

        /* Place the container to the right */
        .right {
            padding: 0px 0px 20px 40px;
            left: 50%;
        }

        /* Add arrows to the left container (pointing right) */
        .left::before {
            content: " ";
            position: absolute;
            top: 18px;
            z-index: 1;
            right: 30px;
            border: medium solid white;
            border-width: 10px 0 10px 10px;
            border-color: transparent transparent transparent white;
        }

        /* Add arrows to the right container (pointing left) */
        .right::before {
            content: " ";
            position: absolute;
            top: 18px;
            z-index: 1;
            left: 30px;
            border: medium solid white;
            border-width: 10px 10px 10px 0;
            border-color: transparent white transparent transparent;
        }

        /* Fix the circle for containers on the right side */
        .right::after {
            left: -12px;
        }

        /* Media queries - Responsive timeline on screens less than 600px wide */
        @media screen and (max-width: 600px) {

            /* Place the timelime to the left */
            .main-timeline::after {
                left: 31px;
            }

            /* Full-width containers */
            .timeline {
                width: 100%;
                padding-left: 70px;
                padding-right: 25px;
            }

            /* Make sure that all arrows are pointing leftwards */
            .timeline::before {
                left: 60px;
                border: medium solid white;
                border-width: 10px 10px 10px 0;
                border-color: transparent white transparent transparent;
            }

            /* Make sure all circles are at the same spot */
            .left::after,
            .right::after {
                left: 18px;
            }

            .left::before {
                right: auto;
            }

            /* Make all right containers behave like the left ones */
            .right {
                left: 0%;
            }
        }
    </style>
@endpush


@section('content')
    @include('layouts.dashboard.main-headerbar')

    <main class="main mt-5 pt-3">
        <section>
            <div class="container-fluid">
                <div class="row ">
                    @include('layouts.dashboard.doctor-sidebar')
                    <div class="col-10 col-sm-10  col-md-10 col-lg-10 col-xl-10 py-3 mt-3">

                        <div>
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="PatientRecord-tab" data-bs-toggle="tab"
                                        data-bs-target="#PatientRecord" type="button" role="tab"
                                        aria-controls="PatientRecord" aria-selected="true">Patient Record</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="X-Rays-tab" data-bs-toggle="tab" data-bs-target="#X-Rays"
                                        type="button" role="tab" aria-controls="X-Rays"
                                        aria-selected="false">X-Rays</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="Laboratories-tab" data-bs-toggle="tab"
                                        data-bs-target="#Laboratories" type="button" role="tab"
                                        aria-controls="Laboratories" aria-selected="false">Laboratories</button>
                                </li>



                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="PatientRecord" role="tabpanel"
                                    aria-labelledby="PatientRecord-tab">
                                    <div class="border border-2 m-3">
                                        <section style="background-color: #F0F2F5;">
                                            <div class="container py-5">
                                                <div class="main-timeline">
                                                    @foreach ($patient_records as $patient_record)
                                                        @if ($loop->even)
                                                        <div class="timeline left">
                                                            <div class="card">
                                                                <div class="card-body p-4">
                                                                    <h3>{{ $patient_record->date }}</h3>
                                                                    <h3>{{ $patient_record->doctor->full_name }}</h3>
                                                                    <p class="mb-0">{{ $patient_record->diagnose }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if ($loop->odd)
                                                        <div class="timeline right">
                                                            <div class="card">
                                                                <div class="card-body p-4">
                                                                    <h5>Diagnose Date: {{ $patient_record->date }}</h5>
                                                                    <h5>The Doctor: {{ $patient_record->doctor->full_name }}</h5>
                                                                    <p class="mb-0">{{ $patient_record->diagnose }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif

                                                    @endforeach



                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="X-Rays" role="tabpanel" aria-labelledby="X-Rays-tab">
                                    <div class="border border-2 m-3">
                                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>request Description</th>
                                                    <th>Patient Name</th>
                                                    <th>Doctor Name</th>
                                                    <th>Invoice Date</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($patient_rays as $patient_ray)
                                                    <tr>
                                                        <td>{{ $patient_ray->id }}</td>
                                                        <td>{{ $patient_ray->description }}</td>
                                                        <td><a
                                                                href="{{ route('Invoices.show', [$patient_ray->id]) }}">{{ $patient_ray->patient->full_name }}</a>
                                                        </td>
                                                        <td>{{ $patient_ray->doctor->full_name }}</td>
                                                        <td>{{ $patient_ray->created_at }}</td>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                </tr>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="Laboratories" role="tabpanel"
                                    aria-labelledby="Laboratories-tab">
                                    <div class="border border-2 m-3">
                                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>request Description</th>
                                                    <th>Patient Name</th>
                                                    <th>Doctor Name</th>
                                                    <th>Invoice Date</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($patient_labs as $patient_lab)
                                                    <tr>
                                                        <td>{{ $patient_lab->id }}</td>
                                                        <td>{{ $patient_lab->description }}</td>
                                                        <td><a
                                                                href="{{ route('Invoices.show', [$patient_lab->id]) }}">{{ $patient_lab->patient->full_name }}</a>
                                                        </td>
                                                        <td>{{ $patient_lab->doctor->full_name }}</td>
                                                        <td>{{ $patient_lab->created_at }}</td>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                </tr>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    {{-- @if (empty($invoices))

                    @endif --}}
                    {{-- @foreach ($doctors as $doctor)
                         @include('dashboard.Doctors.edit')
                         @include('dashboard.Doctors.delete')
                    @endforeach --}}

                </div>

            </div>
        </section>

    </main>
@endsection


@push('scripts')
    <script defer src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script defer src="https://cdn.datatables.net/2.0.4/js/dataTables.js"></script>
    <script defer src="https://cdn.datatables.net/2.0.4/js/dataTables.bootstrap4.js"></script>
@endpush
