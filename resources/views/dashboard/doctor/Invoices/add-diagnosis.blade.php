

<!-- Modal -->
<div class="modal fade" id="add-diagnosis" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add diagnosis</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ route('Diagnosis.store') }}">
            @csrf
        <div class="modal-body">
            <input type="hidden" name="invoice_id"  value="{{$invoice->id}}">
            <input type="hidden" name="patient_id"  value="{{$invoice->patient_id}}">
            <input type="hidden" name="doctor_id"  value="{{$invoice->doctor_id}}">
                <div class="form-row">
                    <div class="col-lg-7 w-100 mx-auto">
                        <div class="form-group mb-2">
                            <label for="Diagnosis">Diagnosis:</label>
                           <textarea  class="form-control"  placeholder="diagnosis"
                           name="diagnosis" rows="3"></textarea>
                        </div>
                    </div>

                    <div class="col-lg-7 w-100 mx-auto">
                        <div class="form-group mb-2">
                            <label for="description">Medicine:</label>
                          <textarea  class="form-control"  placeholder="medicine"
                           name="medicine" rows="3"></textarea>
                        </div>
                    </div>
                    {{-- <div class="col-7 w-100 text-center">
                        <button type="submit" class="btn btn-primary mb-2 w-75" >save</button>
                    </div> --}}

                </div>

                </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
      </div>
    </div>
  </div>
