@extends('headSection')

@section('title')
    login Page
@endsection
@push('styles')
    <style>
        .or {
            position: relative;
        }

        .or::before {
            content: "";
            display: block;
            width: 120px;
            height: 1px;
            position: absolute;
            background: black;
            opacity: 0.3;
            left: 0;
            top: 50%;
        }

        .or::after {
            content: "";
            display: block;
            width: 120px;
            height: 1px;
            position: absolute;
            background: black;
            opacity: 0.3;
            right: 0;
            top: 50%;
        }

        .LoginForm {
            display: none;
        }
    </style>
@endpush
@section('content')
    <div class="d-flex">
        <div class="container m-auto w-50 my-5">
            <div class="row bg-white ">

                <div class="col-sm-12 col-md-12 col-lg-5 p-0 me-2">
                    <img src="images/nurse2.jpg" alt="login picture" height="100%" width="100%">
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6   pt-5">
                    <div class="text-center">
                        <img src="images/HospitaLogo.png" height="100px" width="100px">
                        <h5>Log In </h5>

                    </div>
                    <div class="form-group">
                        <label class="ms-5 mb-2" id="" for="">
                            Example select

                        </label>
                        <select class="form-control w-75 mx-auto mb-2 " id="sectionChooser">
                            <option value="" selected disabled> select from menu</option>

                            <option value="admin">login as admin</option>
                            <option value="doctor">login as doctor</option>
                            <option value="rayEmployee">login as ray employee</option>
                            <option value="laboratoryEmployee">login as laboratory employee</option>
                            <option value="patient">login as patient</option>
                        </select>
                    </div>


                    {{-- admin form --}}
                    <div class="LoginForm" id="admin">
                        <p class="opacity-75 text-center">Get acces to your account</p>
                        <div class="border border-2 w-75 mx-auto my-2 py-1 text-center">
                            <i class="bi bi-google" style="color: red"></i>
                            <a href="{{ route('google.redirect') }}" class="btn ">Login with Google</a>
                        </div>
                        <div class="border border-2 w-75 mx-auto my-2 py-1 text-center">
                            <i class="bi bi-facebook" style="color: blue"></i>
                            <a href="#" class="btn ">Login with Facebook</a>
                        </div>

                        <p class="w-75 mx-auto text-center or">
                            <span class="Or_span">
                                OR
                            </span>
                        </p>
                        <form method="POST" action="{{ route('login.admin') }}">
                            @csrf
                            <div class="form-row">
                                <div class="col-lg-7 w-75 mx-auto">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="bi bi-person-fill"></i></div>
                                        </div>
                                        <input type="text" id="email" class="form-control" placeholder="Username"
                                            type="email" name="email" :value="old('email')" required autofocus
                                            autocomplete="username">
                                    </div>
                                </div>
                                <div class="col-7 w-75 mx-auto">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="bi bi-lock-fill"></i></div>
                                        </div>
                                        <input type="password" class="form-control" id="password" type="password"
                                            name="password" required autocomplete="current-password"
                                            placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-7 m-auto w-75  mb-4">
                                    <div class="container-fluid d-flex justify-content-between p-0">
                                        <div class="d-inline">
                                            <input class="" type="checkbox" id="remember_me">
                                            <label class="" for="remember_me">
                                                Remember me
                                            </label>
                                        </div>
                                        <div class="d-inline">
                                            <a href="{{ route('password.request') }}" class="text-primary">
                                                Forget Password?
                                            </a>
                                        </div>
                                    </div>


                                </div>
                                <div class="col-7 w-100 text-center">
                                    <button type="submit" class="btn btn-primary mb-2 w-75">Log in</button>
                                </div>

                            </div>
                        </form>
                        <div class="mt-2 text-center">
                            <p>Don't have an account?<a href="{{ route('register') }}" class="text-primary">Sign Up</a>
                            </p>
                        </div>
                    </div>

                    {{-- doctor form --}}
                    <div class="LoginForm" id="doctor">
                        <p class="opacity-75 text-center">Get acces to your account</p>
                        <div class="border border-2 w-75 mx-auto my-2 py-1 text-center">
                            <i class="bi bi-google" style="color: red"></i>
                            <a href="#">Login with Google</a>
                        </div>
                        <div class="border border-2 w-75 mx-auto my-2 py-1 text-center">
                            <i class="bi bi-facebook" style="color: blue"></i>
                            <a href="#">Login with Facebook</a>
                        </div>

                        <p class="w-75 mx-auto text-center or">
                            <span class="Or_span">
                                OR
                            </span>
                        </p>
                        <form method="POST" action="{{ route('login.doctor') }}">
                            @csrf
                            <div class="form-row">
                                <div class="col-lg-7 w-75 mx-auto">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="bi bi-person-fill"></i></div>
                                        </div>
                                        <input type="text" id="email" class="form-control" placeholder="Username"
                                            type="email" name="email" :value="old('email')" required autofocus
                                            autocomplete="username">
                                    </div>
                                </div>
                                <div class="col-7 w-75 mx-auto">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="bi bi-lock-fill"></i></div>
                                        </div>
                                        <input type="password" class="form-control" id="password" type="password"
                                            name="password" required autocomplete="current-password"
                                            placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-7 m-auto w-75  mb-4">
                                    <div class="container-fluid d-flex justify-content-between p-0">
                                        <div class="d-inline">
                                            <input class="" type="checkbox" id="remember_me">
                                            <label class="" for="remember_me">
                                                Remember me
                                            </label>
                                        </div>
                                        <div class="d-inline">
                                            <a href="{{ route('password.request') }}" class="text-primary">
                                                Forget Password?
                                            </a>
                                        </div>
                                    </div>


                                </div>
                                <div class="col-7 w-100 text-center">
                                    <button type="submit" class="btn btn-primary mb-2 w-75">Log in</button>
                                </div>

                            </div>
                        </form>
                        <div class="mt-2 text-center">
                            <p>Don't have an account?<a href="{{ route('register') }}" class="text-primary">Sign Up</a>
                            </p>
                        </div>
                    </div>

                    {{-- ray employee form --}}
                    <div class="LoginForm" id="rayEmployee">
                        <p class="opacity-75 text-center">Get acces to your account</p>
                        <div class="border border-2 w-75 mx-auto my-2 py-1 text-center">
                            <i class="bi bi-google" style="color: red"></i>
                            <a href="#">Login with Google</a>
                        </div>
                        <div class="border border-2 w-75 mx-auto my-2 py-1 text-center">
                            <i class="bi bi-facebook" style="color: blue"></i>
                            <a href="#">Login with Facebook</a>
                        </div>

                        <p class="w-75 mx-auto text-center or">
                            <span class="Or_span">
                                OR
                            </span>
                        </p>
                        <form method="POST" action="{{ route('login.rayEmployee') }}">
                            @csrf
                            <div class="form-row">
                                <div class="col-lg-7 w-75 mx-auto">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="bi bi-person-fill"></i></div>
                                        </div>
                                        <input type="text" id="email" class="form-control" placeholder="Username"
                                            type="email" name="email" :value="old('email')" required autofocus
                                            autocomplete="username">
                                    </div>
                                </div>
                                <div class="col-7 w-75 mx-auto">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="bi bi-lock-fill"></i></div>
                                        </div>
                                        <input type="password" class="form-control" id="password" type="password"
                                            name="password" required autocomplete="current-password"
                                            placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-7 m-auto w-75  mb-4">
                                    <div class="container-fluid d-flex justify-content-between p-0">
                                        <div class="d-inline">
                                            <input class="" type="checkbox" id="remember_me">
                                            <label class="" for="remember_me">
                                                Remember me
                                            </label>
                                        </div>
                                        <div class="d-inline">
                                            <a href="{{ route('password.request') }}" class="text-primary">
                                                Forget Password?
                                            </a>
                                        </div>
                                    </div>


                                </div>
                                <div class="col-7 w-100 text-center">
                                    <button type="submit" class="btn btn-primary mb-2 w-75">Log in</button>
                                </div>

                            </div>
                        </form>
                        <div class="mt-2 text-center">
                            <p>Don't have an account?<a href="{{ route('register') }}" class="text-primary">Sign Up</a>
                            </p>
                        </div>
                    </div>

                    {{-- Laboratory employee form --}}
                    <div class="LoginForm" id="laboratoryEmployee">
                        <p class="opacity-75 text-center">Get acces to your account</p>
                        <div class="border border-2 w-75 mx-auto my-2 py-1 text-center">
                            <i class="bi bi-google" style="color: red"></i>
                            <a href="#">Login with Google</a>
                        </div>
                        <div class="border border-2 w-75 mx-auto my-2 py-1 text-center">
                            <i class="bi bi-facebook" style="color: blue"></i>
                            <a href="#">Login with Facebook</a>
                        </div>

                        <p class="w-75 mx-auto text-center or">
                            <span class="Or_span">
                                OR
                            </span>
                        </p>
                        <form method="POST" action="{{ route('login.laboratoryEmployee') }}">
                            @csrf
                            <div class="form-row">
                                <div class="col-lg-7 w-75 mx-auto">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="bi bi-person-fill"></i></div>
                                        </div>
                                        <input type="text" id="email" class="form-control" placeholder="Username"
                                            type="email" name="email" :value="old('email')" required autofocus
                                            autocomplete="username">
                                    </div>
                                </div>
                                <div class="col-7 w-75 mx-auto">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="bi bi-lock-fill"></i></div>
                                        </div>
                                        <input type="password" class="form-control" id="password" type="password"
                                            name="password" required autocomplete="current-password"
                                            placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-7 m-auto w-75  mb-4">
                                    <div class="container-fluid d-flex justify-content-between p-0">
                                        <div class="d-inline">
                                            <input class="" type="checkbox" id="remember_me">
                                            <label class="" for="remember_me">
                                                Remember me
                                            </label>
                                        </div>
                                        <div class="d-inline">
                                            <a href="{{ route('password.request') }}" class="text-primary">
                                                Forget Password?
                                            </a>
                                        </div>
                                    </div>


                                </div>
                                <div class="col-7 w-100 text-center">
                                    <button type="submit" class="btn btn-primary mb-2 w-75">Log in</button>
                                </div>

                            </div>
                        </form>
                        <div class="mt-2 text-center">
                            <p>Don't have an account?<a href="{{ route('register') }}" class="text-primary">Sign Up</a>
                            </p>
                        </div>
                    </div>

                    {{-- patient form --}}
                    <div class="LoginForm" id="patient">
                        <p class="opacity-75 text-center">Get acces to your account</p>
                        <div class="border border-2 w-75 mx-auto my-2 py-1 text-center">
                            <i class="bi bi-google" style="color: red"></i>
                            <a href="#">Login with Google</a>
                        </div>
                        <div class="border border-2 w-75 mx-auto my-2 py-1 text-center">
                            <i class="bi bi-facebook" style="color: blue"></i>
                            <a href="#">Login with Facebook</a>
                        </div>

                        <p class="w-75 mx-auto text-center or">
                            <span class="Or_span">
                                OR
                            </span>
                        </p>
                        <form method="POST" action="{{ route('login.patient') }}">
                            @csrf
                            <div class="form-row">
                                <div class="col-lg-7 w-75 mx-auto">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="bi bi-person-fill"></i></div>
                                        </div>
                                        <input type="text" id="email" class="form-control" placeholder="Username"
                                            type="email" name="email" :value="old('email')" required autofocus
                                            autocomplete="username">
                                    </div>
                                </div>
                                <div class="col-7 w-75 mx-auto">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="bi bi-lock-fill"></i></div>
                                        </div>
                                        <input type="password" class="form-control" id="password" type="password"
                                            name="password" required autocomplete="current-password"
                                            placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-7 m-auto w-75  mb-4">
                                    <div class="container-fluid d-flex justify-content-between p-0">
                                        <div class="d-inline">
                                            <input class="" type="checkbox" id="remember_me">
                                            <label class="" for="remember_me">
                                                Remember me
                                            </label>
                                        </div>
                                        <div class="d-inline">
                                            <a href="{{ route('password.request') }}" class="text-primary">
                                                Forget Password?
                                            </a>
                                        </div>
                                    </div>


                                </div>
                                <div class="col-7 w-100 text-center">
                                    <button type="submit" class="btn btn-primary mb-2 w-75">Log in</button>
                                </div>

                            </div>
                        </form>
                        <div class="mt-2 text-center">
                            <p>Don't have an account?<a href="{{ route('register') }}" class="text-primary">Sign Up</a>
                            </p>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('#sectionChooser').change(function() {
                id = $(this).val();
                console.log(id);

                $('.LoginForm').each(function() {
                    id === $(this).attr('id') ? $(this).show() : $(this).hide();
                });
            });
        });
    </script>
@endpush
