@extends('layouts.dashboard.master')

@section('title')
   Doctor Dashboard
@endsection

@push('css')
    <style>
        body {
            background-color: #fff;
        }

        .card-box {
            background-color: #e0e8eb;
        }

        .card-box-style {
            background-color: rgb(8, 8, 79);
            color: #fff !important;
            box-shadow: 5px 5px 3px 4px lightblue !important;

            @media (min-width:1400px) {
                width: 380px !important;
            }

        }

        .head-color {
            background-color: rgb(8, 8, 79) !important;
        }

        .nav li a {
            color: white !important;
        }
    </style>
@endpush

@section('content')
        @include('layouts.dashboard.main-headerbar')

    <main class="main mt-5 pt-3">
        <section>
            <div class="container-fluid">
                <div class="row ">
                    @include('layouts.dashboard.main-sidebar')
                    <div class="col-10 col-sm-10  col-md-10 col-lg-10 col-xl-10 py-3">
                        <section>
                            <div class="container-fluid">
                                <div class="row mt-2 ms-2">
                                    <div class="col-md-12 fw-bold fs-3"></div>
                                    <h5> Doctor Dashboard</h5>
                                </div>
                                <div class="row mx-2 my-3">
                                    <div class="col-md-6 col-lg-4 mb-3">
                                        <div class="card text-dark mb-3 border border-0 card-box" style="">

                                            <div class="card-body ">
                                                <h5 class="card-title">Members Progress</h5>
                                                <p class="card-text">
                                                <p>
                                                    $72,540
                                                </p>
                                                <p>
                                                    <span class="rounded me-3" style="background-color: #00ce00">
                                                        +9.0%
                                                    </span>
                                                    <span>
                                                        Since Last Month
                                                    </span>
                                                </p>
                                                </p>
                                            </div>
                                            {{-- <div class="card-footer d-flex">
                                                <span>view details</span>
                                                <span class="ms-auto"><i class="bi bi-chevron-right"></i></span>
                                            </div> --}}
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-4 mb-3">
                                        <div class="card text-dark  mb-3 border border-0 card-box" style="">

                                            <div class="card-body ">
                                                <h5 class="card-title">Members Progress</h5>
                                                <p class="card-text">
                                                <p>
                                                    $72,540
                                                </p>
                                                <p>
                                                    <span class="rounded me-3" style="background-color: #00ce00">
                                                        +9.0%
                                                    </span>
                                                    <span>
                                                        Since Last Month
                                                    </span>
                                                </p>
                                                </p>
                                            </div>
                                            {{-- <div class="card-footer d-flex">
                                                <span>view details</span>
                                                <span class="ms-auto"><i class="bi bi-chevron-right"></i></span>
                                            </div> --}}
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-4 mb-3">
                                        <div class="card text-dark mb-3 border border-0 card-box" style="">

                                            <div class="card-body ">
                                                <h5 class="card-title">Members Progress</h5>
                                                <p class="card-text">
                                                <p>
                                                    $72,540
                                                </p>
                                                <p>
                                                    <span class="rounded me-3" style="background-color: #00ce00">
                                                        +9.0%
                                                    </span>
                                                    <span>
                                                        Since Last Month
                                                    </span>
                                                </p>
                                                </p>
                                            </div>
                                            {{-- <div class="card-footer d-flex">
                                                <span>view details</span>
                                                <span class="ms-auto"><i class="bi bi-chevron-right"></i></span>
                                            </div> --}}
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </section>
                        <section>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card border border-0">
                                            <div class="card-header fs-5 border border-0">
                                                Avg. Agent Earnings
                                            </div>
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-hover" style="width:100%">
                                                        <thead class="head-color" style="background-color: rgb(8, 8, 79)">
                                                            <tr>
                                                                <th>#</th>
                                                                <th>First</th>
                                                                <th>Last</th>
                                                                <th>Handle</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>Naima</td>
                                                                <td>Ibrahim</td>
                                                                <td>@mdo</td>
                                                            </tr>
                                                            <tr>
                                                                <td>2</td>
                                                                <td>Maysaa</td>
                                                                <td>Rabaa</td>
                                                                <td>@mdo</td>
                                                            </tr>
                                                            <tr>
                                                                <td>3</td>
                                                                <td>Saja</td>
                                                                <td>shamma</td>
                                                                <td>@mdo</td>
                                                            </tr>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>

    </main>
@endsection


@push('scripts')
    <script>
        $(document).ready(function() {
            $('.card-box').hover(function() {
                    console.log(window.innerWidth);
                    $(this).addClass('card-box-style');
                },
                function() {
                    $(this).removeClass('card-box-style');
                }
            );
        });
    </script>
@endpush
