{{-- add section --}}


<!-- Modal -->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Doctor</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ route('Doctors.store') }}" enctype="multipart/form-data">
            @csrf
        <div class="modal-body">

                <div class="form-row">
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="name">Full Name:</label>
                          <input type="text" id="name" class="form-control"  placeholder="full name"
                           name="full_name"  required autofocus autocomplete="full_name"
                          >
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="email">Email:</label>
                          <input type="text" id="email" class="form-control"  placeholder="email"
                          type="email" name="email"  required autofocus autocomplete="email"
                          >
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="password">Password:</label>
                          <input type="password" id="password" class="form-control"  placeholder="password"
                           name="password"  required autofocus autocomplete="password"
                          >
                        </div>
                    </div>

                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="password">section:</label>
                            <select class="form-select" aria-label="Default select example" id="section_id"
                            name="section_id">
                        @php
                            $sections=App\Models\Section::all();
                        @endphp
                            <option selected>Open this select menu</option>
                            @foreach ($sections as $section)
                                <option value="{{ $section->id }}">{{ $section->name }}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>

                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="price">Price:</label>
                          <input type="text" id="price" class="form-control"  placeholder="price"
                           name="price"  required autofocus autocomplete="price"
                          >
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="phone">Phone:</label>
                          <input type="phone" id="phone" class="form-control"  placeholder="phone"
                           name="phone"  required autofocus autocomplete="phone"
                          >
                        </div>
                    </div>
                    {{-- <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <input class="form-check-input" type="checkbox"  id="status" name="status">
                            <label for="status">status:</label>

                        </div>
                    </div> --}}
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="appointments">appointments:</label>
                          {{-- <input type="text" id="appointments" class="form-control"  placeholder="appointments"
                           name="appointments"  required autofocus autocomplete="appointments"
                          > --}}
                          <select multiple class="form-select" aria-label="Default select example" name="appointments[]"  id="cars" >
                            <option selected>Open this select menu</option>
                            <option >Sunday</option>
                            <option >Monday</option>
                            <option >Tuesday</option>
                            <option >Wednesday</option>
                            <option >Thursday</option>
                            <option >Friday</option>
                            <option >Saturday</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                     <div class="form-group mb-2">
                        <label for="image">Example file input</label>
                        <input name="photo" type="file" class="form-control-file" id="profileImage">
                      </div>
                    </div>
                    {{-- <div class="col-7 w-100 text-center">
                        <button type="submit" class="btn btn-primary mb-2 w-75" >save</button>
                    </div> --}}

                </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
      </div>
    </div>
  </div>
