{{-- edit section --}}
{{-- add section --}}


<!-- Modal -->
<div class="modal fade" id="edit{{$doctor->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">edit Doctor</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ route('Doctors.update',[$doctor->id]) }}" enctype="multipart/form-data">
            {{method_field('patch')}}

            {{ csrf_field() }}
            @csrf
        <div class="modal-body">

                <div class="form-row">
                    <input type="hidden" name="id" value="{{$doctor->id}}">
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="name">Full Name:</label>
                          <input type="text" id="name" class="form-control"  placeholder="full name"
                           name="full_name"  required autofocus autocomplete="full_name" value="{{$doctor->full_name}}"
                          >
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="email">Email:</label>
                          <input type="text" id="email" class="form-control"  placeholder="email"
                          type="email" name="email"  required autofocus autocomplete="email" value="{{$doctor->email}}"
                          >
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="password">Password:</label>
                          <input type="password" id="password" class="form-control"  placeholder="password"
                           name="password"   autofocus
                          >
                        </div>
                    </div>

                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="password">section:</label>
                            <select class="form-select" aria-label="Default select example" id="section_id"
                            name="section_id">
                        @php
                            $sections=App\Models\Section::all();
                        @endphp
                            <option selected>Open this select menu</option>
                            @foreach ($sections as $section)
                                <option value="{{ $section->id }}" {{$section->id==$doctor->section->id ? 'selected':''}}>{{ $section->name }}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>

                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="price">Price:</label>
                          <input type="text" id="price" class="form-control"  placeholder="price"
                           name="price"  required autofocus autocomplete="price" value="{{$doctor->price}}"
                          >
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="phone">Phone:</label>
                          <input type="phone" id="phone" class="form-control"  placeholder="phone"
                           name="phone"  required autofocus autocomplete="phone" value="{{$doctor->phone}}"
                          >
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <input type='hidden' value='off' name='status'>
                            <input class="form-check-input" type="checkbox"  id="status" name="status"
                            {{$doctor->status=='1' ? " value=on checked" :"value=off"}}>
                            <label for="status">status</label>
                          {{-- <input type="text" id="price" class="form-control"  placeholder="price"
                           name="price"  required autofocus autocomplete="price"
                          > --}}
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="appointments">appointments:</label>
                            <select multiple class="form-select" aria-label="Default select example" name="appointments[]"  id="cars" >
                                <option selected>Open this select menu</option>
                                <option >Sunday</option>
                                <option >Monday</option>
                                <option >Tuesday</option>
                                <option >Wednesday</option>
                                <option >Thursday</option>
                                <option >Friday</option>
                                <option >Saturday</option>
                              </select>
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                    <div class="form-group mb-2">
                        <label for="image">Example file input</label>
                        <input type="file" class="form-control-file" id="profileImage" name="photo">
                      </div>
                    </div>
                    {{-- <div class="col-7 w-100 text-center">
                        <button type="submit" class="btn btn-primary mb-2 w-75" >save</button>
                    </div> --}}

                </div>

                </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
      </div>
    </div>
  </div>
