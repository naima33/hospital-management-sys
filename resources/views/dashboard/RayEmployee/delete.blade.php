{{-- delete section --}}
{{-- edit section --}}



<!-- Modal -->
<div class="modal fade" id="delete{{$RayEmployee->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel{{ $RayEmployee->id }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteModalLabel{{ $RayEmployee->id }}">delete Ray Employee</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ route('RayEmployees.destroy',[$RayEmployee->id]) }}">
            {{method_field('delete')}}

            {{ csrf_field() }}
            @csrf
        <div class="modal-body">

             <input type="hidden" name="id" value="{{$RayEmployee->id}}">
             <h5>Are You Sure You Want To Delete This Ray Employee?</h5>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger">Delete</button>
        </div>
    </form>
      </div>
    </div>
  </div>
