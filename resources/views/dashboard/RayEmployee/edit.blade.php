{{-- edit section --}}
{{-- add section --}}


<!-- Modal -->
<div class="modal fade" id="edit{{$RayEmployee->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">edit Ray Employee</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ route('RayEmployees.update',[$RayEmployee->id]) }}">
            {{method_field('patch')}}

            {{ csrf_field() }}
            @csrf
        <div class="modal-body">

                <div class="form-row">
                    <input type="hidden" name="id" value="{{$RayEmployee->id}}">
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="name">Full Name:</label>
                          <input type="text" id="name" class="form-control"  placeholder="full name"
                           name="full_name"  required autofocus autocomplete="full_name" value="{{$RayEmployee->full_name}}"
                          >
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="email">Email:</label>
                          <input type="text" id="email" class="form-control"  placeholder="email"
                          type="email" name="email"  required autofocus autocomplete="email" value="{{$RayEmployee->email}}"
                          >
                        </div>
                    </div>
                    <div class="col-lg-7 w-75 mx-auto">
                        <div class="form-group mb-2">
                            <label for="password">Password:</label>
                          <input type="password" id="password" class="form-control"  placeholder="password"
                           name="password"  required autofocus
                          >
                        </div>
                    </div>
                    {{-- <div class="col-7 w-100 text-center">
                        <button type="submit" class="btn btn-primary mb-2 w-75" >save</button>
                    </div> --}}

                </div>

                </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
      </div>
    </div>
  </div>
