@extends('headSection')

@section('title')
    Register Page
@endsection
@push('styles')
    <style>
        .or {
            position: relative;
        }

        .or::before {
            content: "";
            display: block;
            width: 120px;
            height: 1px;
            position: absolute;
            background: black;
            opacity: 0.3;
            left: 0;
            top: 50%;
        }

        .or::after {
            content: "";
            display: block;
            width: 120px;
            height: 1px;
            position: absolute;
            background: black;
            opacity: 0.3;
            right: 0;
            top: 50%;
        }
    </style>
@endpush
@section('content')
    <div class="d-flex">
        <div class="container m-auto w-50 my-5">
            <div class="row bg-white ">

                <div class="col-sm-12 col-md-12 col-lg-5 p-0 me-2">
                    <img src="images/login.avif" alt="login picture" height="100%" width="100%">
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6   pt-5">
                    <div class="text-center">
                        <img src="images/dragon.jpg" height="100px" width="100px">
                        <h5>Register Admin </h5>
                        <p class="opacity-75">Create An Account</p>
                    </div>


                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="col-lg-7 w-75 mx-auto">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="bi bi-person-fill"></i></div>
                                    </div>
                                    <input type="text" name="name" class="form-control" id="inlineFormInputGroup"
                                        placeholder="Username">
                                </div>
                            </div>
                            <div class="col-7 w-75 mx-auto">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">@</div>
                                    </div>
                                    <input type="email" name="email" class="form-control" id="inlineFormInputGroup"
                                        placeholder="Email">
                                </div>
                            </div>
                            <div class="col-7 w-75 mx-auto">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="bi bi-lock-fill"></i></div>
                                    </div>
                                    <input type="password" name="password" required class="form-control"
                                        id="inlineFormInputGroup" placeholder="Password">
                                </div>
                            </div>

                            <div class="col-7 w-75 mx-auto">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="bi bi-lock-fill"></i></div>
                                    </div>
                                    <input type="password" name="password_confirmation" required class="form-control"
                                        id="inlineFormInputGroup" placeholder="Password">
                                </div>
                            </div>
                            <div class="col-lg-7 w-75 mx-auto">
                                <div class="input-group mb-2">
                                    {{-- <div class="input-group-prepend">
                                        <div class="input-group-text"> <i class="bi bi-person-badge"></i></div>
                                    </div> --}}
                                    <div class="custom-file">
                                      <input type="file" name="photo" class="form-control" id="inputGroupFile01"
                                       style="color:transparent;">
                                      {{-- <label class="custom-file-label" for="inputGroupFile01">Choose file</label> --}}
                                    </div>
                                  </div>
                            </div>


                            <div class="col-7 w-100 text-center">
                                <button type="submit" class="btn btn-primary mb-2 w-75">Submit</button>
                            </div>

                        </div>
                    </form>
                    <div class="mt-2 text-center">
                        <p>Already Have An Account?<a href="{{route('login')}}" class="text-primary">Sign In</a></p>
                    </div>
                </div>

            </div>

        </div>

    </div>
@endsection
