<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
    <!-- Fonts -->
    <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    {{-- <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css"> --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <style>
        /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */
        html {
            line-height: 1.15;
            -webkit-text-size-adjust: 100%
        }

        body {
            margin: 0
        }

        a {
            background-color: transparent
        }

        [hidden] {
            display: none
        }

        html {
            font-family: system-ui, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica Neue, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;
            line-height: 1.5
        }

        *,
        :after,
        :before {
            box-sizing: border-box;
            border: 0 solid #e2e8f0
        }

        a {
            color: inherit;
            text-decoration: inherit
        }

        svg,
        video {
            display: block;
            vertical-align: middle
        }

        video {
            max-width: 100%;
            height: auto
        }

        .bg-white {
            --tw-bg-opacity: 1;
            background-color: rgb(255 255 255 / var(--tw-bg-opacity))
        }

        .bg-gray-100 {
            --tw-bg-opacity: 1;
            background-color: rgb(243 244 246 / var(--tw-bg-opacity))
        }

        .border-gray-200 {
            --tw-border-opacity: 1;
            border-color: rgb(229 231 235 / var(--tw-border-opacity))
        }

        .border-t {
            border-top-width: 1px
        }

        .flex {
            display: flex
        }

        .grid {
            display: grid
        }

        .hidden {
            display: none
        }

        .items-center {
            align-items: center
        }

        .justify-center {
            justify-content: center
        }

        .font-semibold {
            font-weight: 600
        }

        .h-5 {
            height: 1.25rem
        }

        .h-8 {
            height: 2rem
        }

        .h-16 {
            height: 4rem
        }

        .text-sm {
            font-size: .875rem
        }

        .text-lg {
            font-size: 1.125rem
        }

        .leading-7 {
            line-height: 1.75rem
        }

        .mx-auto {
            margin-left: auto;
            margin-right: auto
        }

        .ml-1 {
            margin-left: .25rem
        }

        .mt-2 {
            margin-top: .5rem
        }

        .mr-2 {
            margin-right: .5rem
        }

        .ml-2 {
            margin-left: .5rem
        }

        .mt-4 {
            margin-top: 1rem
        }

        .ml-4 {
            margin-left: 1rem
        }

        .mt-8 {
            margin-top: 2rem
        }

        .ml-12 {
            margin-left: 3rem
        }

        .-mt-px {
            margin-top: -1px
        }

        .max-w-6xl {
            max-width: 72rem
        }

        .min-h-screen {
            min-height: 100vh
        }

        .overflow-hidden {
            overflow: hidden
        }

        .p-6 {
            padding: 1.5rem
        }

        .py-4 {
            padding-top: 1rem;
            padding-bottom: 1rem
        }

        .px-6 {
            padding-left: 1.5rem;
            padding-right: 1.5rem
        }

        .pt-8 {
            padding-top: 2rem
        }

        .fixed {
            position: fixed
        }

        .relative {
            position: relative
        }

        .top-0 {
            top: 0
        }

        .right-0 {
            right: 0
        }

        .shadow {
            --tw-shadow: 0 1px 3px 0 rgb(0 0 0 / .1), 0 1px 2px -1px rgb(0 0 0 / .1);
            --tw-shadow-colored: 0 1px 3px 0 var(--tw-shadow-color), 0 1px 2px -1px var(--tw-shadow-color);
            box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow)
        }

        .text-center {
            text-align: center
        }

        .text-gray-200 {
            --tw-text-opacity: 1;
            color: rgb(229 231 235 / var(--tw-text-opacity))
        }

        .text-gray-300 {
            --tw-text-opacity: 1;
            color: rgb(209 213 219 / var(--tw-text-opacity))
        }

        .text-gray-400 {
            --tw-text-opacity: 1;
            color: rgb(156 163 175 / var(--tw-text-opacity))
        }

        .text-gray-500 {
            --tw-text-opacity: 1;
            color: rgb(107 114 128 / var(--tw-text-opacity))
        }

        .text-gray-600 {
            --tw-text-opacity: 1;
            color: rgb(75 85 99 / var(--tw-text-opacity))
        }

        .text-gray-700 {
            --tw-text-opacity: 1;
            color: rgb(55 65 81 / var(--tw-text-opacity))
        }

        .text-gray-900 {
            --tw-text-opacity: 1;
            color: rgb(17 24 39 / var(--tw-text-opacity))
        }

        .underline {
            text-decoration: underline
        }

        .antialiased {
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale
        }

        .w-5 {
            width: 1.25rem
        }

        .w-8 {
            width: 2rem
        }

        .w-auto {
            width: auto
        }

        .grid-cols-1 {
            grid-template-columns: repeat(1, minmax(0, 1fr))
        }

        @media (min-width:640px) {
            .sm\:rounded-lg {
                border-radius: .5rem
            }

            .sm\:block {
                display: block
            }

            .sm\:items-center {
                align-items: center
            }

            .sm\:justify-start {
                justify-content: flex-start
            }

            .sm\:justify-between {
                justify-content: space-between
            }

            .sm\:h-20 {
                height: 5rem
            }

            .sm\:ml-0 {
                margin-left: 0
            }

            .sm\:px-6 {
                padding-left: 1.5rem;
                padding-right: 1.5rem
            }

            .sm\:pt-0 {
                padding-top: 0
            }

            .sm\:text-left {
                text-align: left
            }

            .sm\:text-right {
                text-align: right
            }
        }

        @media (min-width:768px) {
            .md\:border-t-0 {
                border-top-width: 0
            }

            .md\:border-l {
                border-left-width: 1px
            }

            .md\:grid-cols-2 {
                grid-template-columns: repeat(2, minmax(0, 1fr))
            }
        }

        @media (min-width:1024px) {
            .lg\:px-8 {
                padding-left: 2rem;
                padding-right: 2rem
            }
        }

        @media (prefers-color-scheme:dark) {
            .dark\:bg-gray-800 {
                --tw-bg-opacity: 1;
                background-color: rgb(31 41 55 / var(--tw-bg-opacity))
            }

            .dark\:bg-gray-900 {
                --tw-bg-opacity: 1;
                background-color: rgb(17 24 39 / var(--tw-bg-opacity))
            }

            .dark\:border-gray-700 {
                --tw-border-opacity: 1;
                border-color: rgb(55 65 81 / var(--tw-border-opacity))
            }

            .dark\:text-white {
                --tw-text-opacity: 1;
                color: rgb(255 255 255 / var(--tw-text-opacity))
            }

            .dark\:text-gray-400 {
                --tw-text-opacity: 1;
                color: rgb(156 163 175 / var(--tw-text-opacity))
            }

            .dark\:text-gray-500 {
                --tw-text-opacity: 1;
                color: rgb(107 114 128 / var(--tw-text-opacity))
            }
        }
    </style>

    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }
    </style>
</head>

<body class="antialiased">
    <nav class="navbar navbar-expand-lg bg-dark bg-white fixed-top ">
        <div class="container-fluid">
            <a class="navbar-brand ms-5" href="#"><span class="text-light" style="background-color: rgb(8, 8, 79)"> AL </span>SHAMI</a>
            <button class="navbar-toggler shadow-none border-0" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav justify-content-end flex-grow-1 mx-3 ">
                    <li class="nav-item mx-2">
                        <a class="nav-link active" aria-current="page" href="#">Home</a>
                    </li>
                    <li class="nav-item mx-2 mx-2">
                        <a class="nav-link" href="#">ABOUT</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            SERVICES
                        </a>
                        <ul class="dropdown-menu bg-transparent">
                            <li><a class="dropdown-item" href="#">service 1</a></li>
                            <li><a class="dropdown-item" href="#">service 2</a></li>

                            <li><a class="dropdown-item" href="#">service 3</a></li>
                        </ul>
                    </li>
                    <li class="nav-item mx-2">
                        <a class="nav-link ">PORTIFOPLIO</a>
                    </li>
                    <li class="nav-item mx-2">
                        <a class="nav-link ">TEAM</a>
                    </li>
                    <li class="nav-item mx-2">
                        <a class="nav-link ">CONTACT</a>
                    </li>
                    <li class="nav-item mx-2">
                        <a class="nav-link " href="{{route('login')}}">LOGIN</a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>
    <section class="">
        <div id="carouselExampleCaptions" class="carousel slide">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"
                    aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
                    aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
                    aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item justify-content-end flex-grow-1 active">
                    <img src='images/hospital.jpg' height="700px" class="d-block w-100" alt="...">
                    <div class="carousel-caption">
                        <h5>1</h5>

                    </div>
                </div>
                <div class="carousel-item">
                    <img src='images/emergency.jpg'  height="700px" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>2</h5>

                    </div>
                </div>
                <div class="carousel-item">
                    <img src='images/doctor.avif' height="700px"  class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>3</h5>

                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions"
                data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions"
                data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>

    </section>

    <section class="about">
        <div class="container-fluid bg-white h-200 px-0 pt-5 pb-1 ">
            <div class="bg-white p-5 pb-1">
                <div class="row">
                    <div class="col-4 mb-4" style="width:480px">
                        <img src="images/service.avif" width="100%" height="100%">
                    </div>
                    <div class="col-8 ">
                        <div>
                            <h3>
                                We Provide The Best Quality Service Ever
                            </h3>
                            <p class="fs-5 mt-2">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
                                reprehenderit in
                                voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                                cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                            <button class="text-white p-2" style="background-color: rgb(8, 8, 79)" type="button">Learn More</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="services">
        <div class="container-fluid bg-white h-200 px-0 py-5">
            <div>
                <div class="sec1 text-center mb-4">
                    <h4>Our Services</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
                </div>
                <div class="sec2 mx-5 mt-5">
                    <section class="row gy-3 text-center px-3 d-flex justify-content-center">
                        <div class="col-4 " style="width:350px">
                            <div class="card text-dark " style="background-color: #e0e8eb">
                                <i class="bi bi-subtract text-center " style="font-size: 50px"></i>
                                <div class="card-body">
                                    <h5 class="card-title">Inpatient Services</h5>
                                    <p class="card-text text-center">Lorem ipsum dolor sit amet, consectetur adipiscing
                                        elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation
                                        ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    <a href="#" class=" text-white btn " style="background-color: rgb(8, 8, 79)"> Read More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-4" style="width:350px">
                            <div class="card text-dark " style="background-color: #e0e8eb">
                                <i class="bi bi-slack text-center " style="font-size: 50px"></i>
                                <div class="card-body">
                                    <h5 class="card-title">Psychological Services</h5>
                                    <p class="card-text text-center">Lorem ipsum dolor sit amet, consectetur adipiscing
                                        elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation
                                        ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    <a href="#" class="btn text-white" style="background-color: rgb(8, 8, 79)">Read More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-4 " style="width:350px">
                            <div class="card text-dark " style="background-color: #e0e8eb">
                                <i class="bi bi-playstation text-center " style="font-size: 50px"></i>
                                <div class="card-body">
                                    <h5 class="card-title">Outpatient Services</h5>
                                    <p class="card-text text-center">Lorem ipsum dolor sit amet, consectetur adipiscing
                                        elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation
                                        ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    <a href="#" class="btn text-white" style="background-color: rgb(8, 8, 79)">Read More</a>
                                </div>
                            </div>
                        </div>



                    </section>
                </div>
            </div>
        </div>
    </section>

    <section class="sections">
        <div class="container-fluid bg-white h-200 px-0 py-5">
            <div>
                <div class="sec1 text-center mb-4">
                    <h4>Our Sections</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
                </div>
                <div class="sec2 mx-5 mt-5">
                    <section class="row gy-3 text-center px-3 d-flex justify-content-center">
                        <div class="col-4" style="width:350px">
                            <div class="card p-2">
                                <img class="card-img-top border border-white border-5" src="images/Surgery.avif"
                                    alt="Card image cap" height="300px">
                                <div class="card-body">
                                    <h5 class="card-title">Surgery Department</h5>
                                    <p class="card-text text-center">Lorem ipsum dolor sit amet, consectetur adipiscing
                                        elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation
                                        ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    <a href="#" class="btn text-white" style="background-color: rgb(8, 8, 79)">Learn More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-4" style="width:350px">
                            <div class="card p-2">
                                <img class="card-img-top border border-white border-5" src="images/xray.jpg"
                                    alt="Card image cap" height="300px">
                                <div class="card-body">
                                    <h5 class="card-title">X Ray Department</h5>
                                    <p class="card-text text-center">Lorem ipsum dolor sit amet, consectetur adipiscing
                                        elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation
                                        ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    <a href="#" class="btn text-white" style="background-color: rgb(8, 8, 79)">Learn More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-4" style="width:350px">
                            <div class="card p-2">
                                <img class="card-img-top w-100 h-10 border border-white border-5 "
                                    src="images/lab.webp" alt="Card image cap" height="300px">
                                <div class="card-body">
                                    <h5 class="card-title">Laboratory Department</h5>
                                    <p class="card-text text-center">Lorem ipsum dolor sit amet, consectetur adipiscing
                                        elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation
                                        ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    <a href="#" class="btn text-white" style="background-color: rgb(8, 8, 79)">Learn More</a>
                                </div>
                            </div>
                        </div>



                    </section>
                </div>
            </div>
        </div>
    </section>

    <section class="doctors mb-5">
        <div class="container-fluid bg-white h-200 px-0 py-5">
            <div>
                <div class="sec1 text-center mb-4">
                    <h4>Our Doctors</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
                </div>
                <div class="sec2 mx-5 mt-5">
                    <section class="row gy-3 text-center px-3 d-flex justify-content-center">
                        <div class="col-3" style="width:250px">
                            <div class="card p-2">
                                <img class="card-img-top rounded-circle border border-white border-5"
                                    src="images/doctor1.webp" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Michael Missy</h5>
                                    <p class="card-text text-center">Lorem ipsum dolor sit amet, consectetur adipiscing
                                        elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#" class="btn text-white" style="background-color: rgb(8, 8, 79)">Learn More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-3" style="width:250px">
                            <div class="card p-2">
                                <img class="card-img-top rounded-circle border border-white border-5"
                                    src="images/doctor2.webp" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Sara Siiry</h5>
                                    <p class="card-text text-center">Lorem ipsum dolor sit amet, consectetur adipiscing
                                        elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#" class="btn text-white" style="background-color: rgb(8, 8, 79)">Learn More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-3" style="width:250px">
                            <div class="card p-2">
                                <img class="card-img-top rounded-circle w-100 h-10 border border-white border-5 "
                                    src="images/doctor3.webp" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Maya Smith</h5>
                                    <p class="card-text text-center">Lorem ipsum dolor sit amet, consectetur adipiscing
                                        elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#" class="btn text-white" style="background-color: rgb(8, 8, 79)">Learn More</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-3" style="width:250px">
                            <div class="card p-2">
                                <img class="card-img-top rounded-circle w-100 h-10 border border-white border-5 "
                                    src="images/doctor4.webp" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Jack Wilson</h5>
                                    <p class="card-text text-center">Lorem ipsum dolor sit amet, consectetur adipiscing
                                        elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#" class="btn text-white" style="background-color: rgb(8, 8, 79)">Learn More</a>
                                </div>
                            </div>
                        </div>

                    </section>
                </div>
            </div>
        </div>
    </section>

    <hr style="color: white">

    <section class="contact mt-5">
        <div class="container-fluid bg-white h-200 px-0 py-5 ">
            <div class="text-center mb-4">
                <h3>
                    Contact Us
                </h3>
                <p class="mt-2">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,<br> sed do eiusmod sed do eiusmod .
                </p>
            </div>
            <div class="container p-auto">
                <form>
                    <input type="email" class="form-control w-75 mx-auto" name="fullName"
                        placeholder="fullname"><br>
                    <input class="form-control w-75 mx-auto" type="email" name="email" placeholder="email"><br>
                    <textarea class="form-control w-75 mx-auto" type="textarea" name="message" placeholder="Message" rows="4"></textarea><br>
                    <button class="form-control w-75 mx-auto border-0 text-white" style="background-color: rgb(8, 8, 79)">
                        Send Now
                    </button><br>
                </form>
            </div>
        </div>
    </section>
</body>

</html>
