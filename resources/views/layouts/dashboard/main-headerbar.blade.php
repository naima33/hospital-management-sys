<nav class="navbar navbar-expand-md fixed-top" style="background-color: #e0e8eb">
    <div class="container-fluid">
        {{-- <button class="btn me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample"
        aria-controls="offcanvasExample" aria-expanded="true">

        <span class="navbar-toggler-icon" data-bs-target="#offcanvasExample"></span>
    </button> --}}
        {{-- <a class="navbar-brand fw-bold" href="#">Dashboard</a> --}}
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <form class="d-flex me-auto ps-3">
                <div class="input-group my-3 my-lg-0 ">
                    <input type="text" class="form-control rounded-0" placeholder="Search..." aria-label="Search"
                        aria-describedby="button-addon2">
                    <button class="btn rounded-0" type="button" id="button-addon2"
                        style="background-color: rgb(142, 142, 217)">
                        Search
                    </button>
                </div>

            </form>
            <ul class="navbar-nav mb-2 mb-lg-0">


                <li class="nav-item dropdown ">
                    <a class="nav-link " href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        <i class="bi bi-person-fill" style="font-size: 25px"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-dark" aria-labelledby="navbarDropdown">

                        <li><a class="dropdown-item" href="{{ url('profile') }}">Profile</a></li>
                        <li><a class="dropdown-item" href="#">Settings</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li>


                            @if (Auth::guard('admin')->check())

                            <form method="POST" action="{{ route('logout.admin') }}">
                                    @csrf
                                    <a class="dropdown-item" href="{{ url('logout.admin') }}"
                                        onclick="event.preventDefault();
                                         this.closest('form').submit();">Logout</a>
                                </form>

                            @elseif(Auth::guard('doctor')->check())

                                <form method="POST" action="{{ route('logout.doctor') }}">
                                    @csrf
                                    <a class="dropdown-item" href="{{ url('logout.doctor') }}"
                                        onclick="event.preventDefault();
                            this.closest('form').submit();">Logout</a>
                                </form>

                                @elseif(Auth::guard('rayEmployee')->check())

                                <form method="POST" action="{{ route('logout.rayEmployee') }}">
                                    @csrf
                                    <a class="dropdown-item" href="{{ url('logout.rayEmployee') }}"
                                        onclick="event.preventDefault();
                            this.closest('form').submit();">Logout</a>
                                </form>

                                @elseif(Auth::guard('laboratoryEmployee')->check())

                                <form method="POST" action="{{ route('logout.laboratoryEmployee') }}">
                                    @csrf
                                    <a class="dropdown-item" href="{{ url('logout.laboratoryEmployee') }}"
                                        onclick="event.preventDefault();
                            this.closest('form').submit();">Logout</a>
                             </form>

                             @elseif(Auth::guard('patient')->check())

                                <form method="POST" action="{{ route('logout.patient') }}">
                                    @csrf
                                    <a class="dropdown-item" href="{{ url('logout.patient') }}"
                                        onclick="event.preventDefault();
                            this.closest('form').submit();">Logout</a>
                             </form>

                             @else

                             <form method="POST" action="{{ route('logout') }}">
                                 @csrf
                                 <a class="dropdown-item" href="{{ url('logout') }}"
                                     onclick="event.preventDefault();
                         this.closest('form').submit();">Logout</a>
                         </form>
                            @endif
                        </li>

                    </ul>
                </li>

            </ul>

        </div>
    </div>
</nav>
