<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- start data table --}}


    {{-- end data table --}}
    @include('layouts.dashboard.head')

</head>
<body>
	<div>
        @include('layouts.dashboard.main-headerbar')
		@yield('content')
	</div>
    @stack('scripts')
</body>
</html>
