<div class="col-2 col-sm-2  col-md-2 col-xl-2 px-sm-2 px-0 " style="background-color: rgb(8, 8, 79)">
    <div
        class="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100">
        <a href="/"
            class="d-flex align-items-center pb-3 mb-md-0 me-md-auto text-white text-decoration-none">
            <span class=" me-3"> <i class="fs-4 bi bi-list"></i></span>
            <span class="fs-5 d-none d-sm-inline">Menu</span>

        </a>
        <ul class="nav nav-pills flex-column mb-sm-auto mb-0  text-white w-100"
            id="menu">


            <li>
                <a href="#submenu1" data-bs-toggle="collapse" class="nav-link px-0 align-middle d-flex">
                    <i class="fs-4 bi bi-shield-check"></i>
                     <span class="ms-2 d-none d-sm-inline mt-1">Statements</span>
                     <span class="d-inline-flex ms-auto mt-1">
                        {{-- @if ($activate) --}}
                            <i class="bi bi-chevron-down"></i>
                        {{-- @else
                            <i class="bi bi-chevron-up"></i>
                        @endif --}}

                    </span>
                </a>
                <ul class="collapse nav flex-column ms-1 " id="submenu1" data-bs-parent="#menu">
                    <li class="w-100 ps-3">
                        <a href="{{url('invoices')}}" class="nav-link px-0"> <span
                                class="d-none d-sm-inline"></span>  الكشوفات </a>
                    </li>

                    <li class="w-100 ps-3">
                        <a href="{{ url('laboratories') }}" class="nav-link px-0"> <span
                                class="d-none d-sm-inline"></span>  المختبر </a>
                    </li>
                    <li class="w-100 ps-3">
                        <a href="{{ url('rays') }}" class="nav-link px-0"> <span
                                class="d-none d-sm-inline"></span>  الأشعة </a>
                    </li>
                </ul>
            </li>




        </ul>
        <hr>
        <div class="dropdown pb-4">
            @if (Auth::guard('patient')->check())
            <a href="#" class="d-flex align-items-center text-white text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                {{-- <img src="https://github.com/mdo.png" alt="hugenerd" width="30" height="30" class="rounded-circle"> --}}
                <img class="img-responsive rounded-circle" alt="hugenerd" width="30" height="30"
                src="{{ !empty(Auth::guard('patient')->user()->image->filename)  ? asset('storage/patients/' . Auth::guard('patient')->user()->image->filename) : '' }}">
                <span class="d-none d-sm-inline mx-1">{{Auth::guard('patient')->user()->full_name}}</span>

            </a>
            @else
            <a href="#" class="d-flex align-items-center text-white text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                {{-- <img src="https://github.com/mdo.png" alt="hugenerd" width="30" height="30" class="rounded-circle"> --}}
                <img class="img-responsive rounded-circle" alt="hugenerd" width="30" height="30"
                src="{{ !empty(Auth::guard('web')->user()->image->filename)  ? asset('storage/patients/' . Auth::guard('web')->user()->image->filename) : '' }}">
                <span class="d-none d-sm-inline mx-1">{{Auth::guard('web')->user()->name}}</span>

            </a>
            @endif
            <ul class="dropdown-menu dropdown-menu-dark text-small shadow">
                <li><a class="dropdown-item" href="#">New project...</a></li>
                <li><a class="dropdown-item" href="#">Settings</a></li>
                <li><a class="dropdown-item" href="{{ url('profile') }}">Profile</a></li>
                <li>
                    <hr class="dropdown-divider">
                </li>
                <li>
                    <form method="POST" action="{{ route('logout.patient') }}">
                        @csrf
                    <a class="dropdown-item" href="{{ url('logout.patient') }}" onclick="event.preventDefault();
                    this.closest('form').submit();">Sign out</a>
                </form>
                </li>
            </ul>
        </div>
    </div>
</div>
