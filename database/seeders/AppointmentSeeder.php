<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AppointmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('appointments')->insert([
            'name' => 'Saturday',
        ]);
        DB::table('appointments')->insert([
            'name' => 'Sunday',
        ]);
        DB::table('appointments')->insert([
            'name' => 'monday',
        ]);
        DB::table('appointments')->insert([
            'name' => 'tuswday',
        ]);
        DB::table('appointments')->insert([
            'name' => 'wednesday',
        ]);
        DB::table('appointments')->insert([
            'name' => 'thersday',
        ]);
        DB::table('appointments')->insert([
            'name' => 'Friday',
        ]);
    }
}
